"""
Toy example of a feedback loop to reduce intensity transfer function of a
laser.

Here we just have a laser incident on a photodiode. The photodiode output has
multiple filters applied to it then it is fed back into the amplitude
modulation input of the laser.

The gains of the feedback is then varied to see which combinations increase or
decrease the intensity coupling.

A plot of the closed loop RIN to photodiode output is plotted against feedback
gain, with varying phase.

You should see an unstable system when the gain > 1 and the phase is incorrect,
as positive feedback is applied.
"""

import finesse
import matplotlib.pyplot as plt
import numpy as np

from finesse import Model
from finesse.components import Laser, Photodiode, FilterZPK, SignalGenerator
from scipy.signal import butter

finesse.plotting.style.use('default')

ifo = Model()
ifo.fsig.f = 10  # [Hz]

l1 = Laser('l1', P=1)
pd = Photodiode('pd')

# Some filter to scale the feedback
F1 = FilterZPK('F1', butter(2, 2 * np.pi * 10, 'lp', output='zpk', analog=True))
F2 = FilterZPK('F2', ([], [], 1))

# Signal generator we'll use to drive the laser ampslitude modulation
ifo.add(SignalGenerator('sg', l1.amp_sig.i))

ifo.connect(l1, pd)
ifo.connect(pd.p2, F1.p1)
ifo.connect(F1.p2, F2.p1)
ifo.connect(F2.p2, l1.amp_sig)

ifo.fsig.f.is_tunable = True
carrier, signal = ifo.build()

f = np.logspace(0, 3, 1000)

fig1 = plt.figure(figsize=(10, 8))
ax1 = plt.subplot(211)
ax2 = plt.subplot(212)

def run():
    res = []
    for ifo.fsig.f in f:
        signal.run(carrier)
        E = signal.out[signal.field(ifo.pd.p2.o)]  # Get photodiode output
        res.append(E)
    return res


with carrier, signal:
    # Solve the carrier fields
    carrier.run()

    F2.zpk[2] = 1e-20
    res = run()
    ax1.loglog(f, np.abs(res), label='Open loop')
    ax2.semilogx(f, np.angle(res, True))

    F2.zpk[2] = 1
    res = run()
    ax1.loglog(f, np.abs(res), label='G=1, LP=100Hz')
    ax2.semilogx(f, np.angle(res, True))

    F2.zpk[2] = -1
    res = run()
    ax1.loglog(f, np.abs(res), label='G=-1, LP=100Hz')
    ax2.semilogx(f, np.angle(res, True))

    F2.zpk[2] = -10
    res = run()
    ax1.loglog(f, np.abs(res), label='G=-10, LP=100Hz')
    ax2.semilogx(f, np.angle(res, True))

    F1.zpk = list(butter(4, 2 * np.pi * 10, 'lp', output='zpk', analog=True))
    F2.zpk[2] = -100
    res = run()
    ax1.loglog(f, np.abs(res), label='G=-100, 4th order filter, LP=100Hz')
    ax2.semilogx(f, np.angle(res, True))

    F1.zpk = list(butter(4, 2 * np.pi * 100, 'hp', output='zpk', analog=True))
    F2.zpk[2] = -10
    res = run()
    ax1.loglog(f, np.abs(res), label='G=-10, 4th order Butterworth, HP=100Hz')
    ax2.semilogx(f, np.angle(res, True))

ax1.set_title('Closed loop RIN with varying feedback gain (G) and filter design')
ax1.legend(ncol=2)

ax1.set_xlabel("Frequency [Hz]")
ax1.set_ylabel('Laser RIN [W/W]')
ax2.set_xlabel("Frequency [Hz]")
ax2.set_ylabel('Phase [deg]')

plt.tight_layout()
plt.show()
