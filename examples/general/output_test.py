import finesse
import matplotlib.pyplot as plt
import numpy as np
import h5py

from finesse import Model, Output
from finesse.components import Laser, Mirror, Photodiode
from scipy.signal import butter
from finesse.detectors import PowerDetector

finesse.plotting.style.use('default')

ifo = Model()

l1  = Laser('l1',  P=1)
m1  = Mirror('m1', T=0.5, R=0.5 )
pd1 = Photodiode('pd1')

ifo.connect(l1, m1)
ifo.connect(m1, pd1)

ifo.add( PowerDetector('Pt', m1.p2.o) )
ifo.add( PowerDetector('Pr', m1.p1.o) )

l1.P.is_tunable = True
m1.R.is_tunable = True

carrier, = ifo.build()

out1D = Output(ifo, 10)
out1D.x = np.linspace(0, 1, 10)

with carrier:
    for i, x in enumerate(out1D.x):
        l1.P = x
        carrier.run()
        out1D.update(i)

out2D = Output(ifo, (10,10))
out2D.x = np.linspace(0, 1, 10)
out2D.y = np.linspace(3, 100, 5)

with carrier:
    for i, x in enumerate(out2D.x):
        m1.R = x
        for j, y in enumerate(out2D.y):
            l1.P = y
            carrier.run()
            out2D.update((i,j))
