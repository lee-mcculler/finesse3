"""
Simple test to see if a variable can be added and then used in another model parameter
"""

import finesse
from finesse.parse import KatParser
from finesse.utilities.xaxis import xaxis, noxaxis

k = KatParser()

k.parse("""
l l1 0 0 n0
pd pd1 n0
""")

ifo = k.build()

x = finesse.components.Variable('x', 10)
ifo.add(x)

ifo.l1.P = x.value.ref**2