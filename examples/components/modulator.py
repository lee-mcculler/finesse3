import finesse
from finesse.parse import KatParser
from finesse.utilities.xaxis import noxaxis

k = KatParser()
k.parse(path="modulator.kat")

ifo = k.build()
out = noxaxis(ifo)

#finesse.init_plotting()
#out.plot() 