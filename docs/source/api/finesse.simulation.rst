==========================================
``finesse.simulation`` --- Model execution
==========================================

.. automodule:: finesse.simulation

.. currentmodule:: finesse.simulation

Classes
=======

.. autosummary::
    :toctree: simulation

    BaseSimulation
    KLUSimulation
    Output
