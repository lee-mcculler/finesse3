=================================================
``finesse.freeze`` --- Freezable object utilities
=================================================

.. automodule:: finesse.freeze

Functions
=========

.. autosummary::
    :toctree: generated/

    canFreeze

Classes
=======
.. currentmodule:: finesse.freeze
.. autoclass:: Freezable

--------

``Freezable`` Methods
=====================

.. rubric:: Constructing a freezable instance

.. autosummary::
    :toctree: generated/

    Freezable.__init__
