===================================================
``finesse.element.parameter`` --- Parameter classes
===================================================

.. currentmodule:: finesse.element

Overview
========

.. autoclass:: parameter
.. autoclass:: Parameter

``Parameter`` properties
========================

.. autosummary::
    :toctree: generated/

    Parameter.ref
    Parameter.is_changing
    Parameter.is_tunable
    Parameter.value
    Parameter.name
    Parameter.component
    Parameter.owner
    Parameter.locked

``Parameter`` methods
=====================

.. autosummary::
    :toctree: generated/

    Parameter.__init__
    Parameter.eval
