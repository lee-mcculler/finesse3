===================================================
``finesse.frequency.Frequency`` --- Frequency class
===================================================

.. currentmodule:: finesse.frequency

Overview
========

.. autoclass:: Frequency

Properties
==========

.. autosummary::
    :toctree: generated/

    Frequency.audio_order
    Frequency.audio_carrier_index
    Frequency.f
    Frequency.index
    Frequency.is_audio
    Frequency.name
    Frequency.simulation
    Frequency.symbol

Methods
=======

.. rubric:: Constructing a frequency object

.. autosummary::
    :toctree: generated/

    Frequency.__init__
