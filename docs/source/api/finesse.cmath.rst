===================================================
``finesse.cmath`` --- C-based mathematical routines
===================================================

.. automodule:: finesse.cmath
    :members:
