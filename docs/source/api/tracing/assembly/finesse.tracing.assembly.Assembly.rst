========================================================
``finesse.tracing.assembly.Assembly`` --- Assembly class
========================================================

.. currentmodule:: finesse.tracing.assembly

Overview
========

.. autoclass:: Assembly

Methods
=======

.. rubric:: Constructing an assembly class

.. autosummary::
    :toctree: generated/

    Assembly.__init__

.. rubric:: Parameters of the optimiser

.. autosummary::
    :toctree: generated/

    Assembly.allow
    Assembly.target

.. rubric:: Executing the minimisation routine

.. autosummary::
    :toctree: generated/

    Assembly.solve
