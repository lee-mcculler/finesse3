===========================================
``finesse.frequency`` --- Frequency classes
===========================================

.. automodule:: finesse.frequency

.. currentmodule:: finesse.frequency

Classes
=======

.. autosummary::
    :toctree: frequency

    Frequency

Functions
=========

.. autosummary::
    :toctree: generated/

    generate_frequency_list
