========================================================
``finesse.components.general.Surface`` --- Surface class
========================================================

.. currentmodule:: finesse.components.general

Overview
========

.. autoclass:: Surface

Properties
==========

.. autosummary::
    :toctree: generated/

    Surface.mass
    Surface.Rc
