==============================================================================
``finesse.components.general.FrequencyGenerator`` --- FrequencyGenerator class
==============================================================================

.. currentmodule:: finesse.components.general

Overview
========

.. autoclass:: FrequencyGenerator

Methods
=======

.. rubric Constructing a frequency generator

.. autosummary::
    :toctree: generated/

    FrequencyGenerator.__init__
