.. _components_general:

===========================================================
``finesse.components.general`` --- Base component interface
===========================================================

.. automodule:: finesse.components.general
.. currentmodule:: finesse.components.general

Enums
=====

.. autoclass:: CouplingType
.. autoclass:: InteractionType
.. autoclass:: SurfaceType

Functions
=========

.. autosummary::
    :toctree: generated/

    determine_coupling_type

Classes
=======

.. autosummary::
    :toctree: general/

    Connector
    FrequencyGenerator
    Surface
