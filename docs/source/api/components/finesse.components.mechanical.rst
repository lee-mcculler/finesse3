.. _components_mechanical:

===========================================================================
``finesse.components.mechanical`` --- Mechanical components and connections
===========================================================================

.. automodule:: finesse.components.mechanical

Overview
========
.. currentmodule:: finesse.components.mechanical
.. autoclass:: Joint
    :show-inheritance:
.. autoclass:: PZT
    :show-inheritance:

Methods
=======

.. rubric:: Constructing a joint

.. autosummary::
    :toctree: generated/

    Joint.__init__
    Joint.connect
