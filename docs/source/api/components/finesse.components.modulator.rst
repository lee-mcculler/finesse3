.. _components_modulator:

======================================================================
``finesse.components.modulator`` --- Modulator type optical components
======================================================================

.. automodule:: finesse.components.modulator

Overview
========
.. currentmodule:: finesse.components.modulator
.. autoclass:: Modulator
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated/

    Modulator.f
    Modulator.phase
    Modulator.order

Methods
=======

.. rubric:: Constructing a modulator

.. autosummary::
    :toctree: generated/

    Modulator.__init__