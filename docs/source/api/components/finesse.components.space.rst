.. _components_space:

======================================================================
``finesse.components.space`` --- Space-type objects between components
======================================================================

.. automodule:: finesse.components.space

Overview
========
.. currentmodule:: finesse.components.space
.. autoclass:: Space
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated/

    Space.L
    Space.nr

Methods
=======

.. rubric:: Constructing a space

.. autosummary::
    :toctree: generated/

    Space.__init__
    Space.connect

.. rubric:: Beam parameter tracing

.. autosummary::
    :toctree: generated/

    Space.ABCD
