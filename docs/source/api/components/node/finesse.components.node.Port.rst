===============================================
``finesse.components.node.Port`` --- Port class
===============================================

.. currentmodule:: finesse.components.node

Overview
========

.. autoclass:: Port

Properties
==========

.. autosummary::
    :toctree: generated/

    Port.attached_to
    Port.component
    Port.is_connected
    Port.name
    Port.nodes
    Port.type

Methods
=======

.. rubric:: Constructing a port

.. autosummary::
    :toctree: generated/

    Port.__init__
