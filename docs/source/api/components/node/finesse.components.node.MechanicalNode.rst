===================================================================
``finesse.components.node.MechanicalNode`` --- MechanicalNode class
===================================================================

.. currentmodule:: finesse.components.node

Overview
========

.. autoclass:: MechanicalNode

Methods
=======

.. rubric:: Constructing a mechanical node

.. autosummary::
    :toctree: generated/

    MechanicalNode.__init__
