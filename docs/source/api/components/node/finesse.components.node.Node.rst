===============================================
``finesse.components.node.Node`` --- Node class
===============================================

.. currentmodule:: finesse.components.node

Overview
========

.. autoclass:: Node

Properties
==========

.. autosummary::
    :toctree: generated/

    Node.component
    Node.direction
    Node.full_name
    Node.name
    Node.port_name
    Node.type

Methods
=======

.. rubric:: Constructing a node

.. autosummary::
    :toctree: generated/

    Node.__init__
