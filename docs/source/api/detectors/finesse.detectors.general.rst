.. _detectors_general:

==============================================================
``finesse.detectors.general`` --- Top-level, general detectors
==============================================================

.. automodule:: finesse.detectors.general

Overview
========
.. currentmodule:: finesse.detectors.general
.. autoclass:: Detector
    :show-inheritance:

Methods
=======

.. rubric:: Constructing a detector

.. autosummary::
    :toctree: generated/

    Detector.__init__

.. rubric:: Computing the output

.. autosummary::
    :toctree: generated/

    Detector.get_output
