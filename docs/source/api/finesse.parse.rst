======================================
``finesse.parse`` --- Kat file parsing
======================================

.. automodule:: finesse.parse

Classes
=======
.. currentmodule:: finesse.parse

.. autoclass:: KatParser

--------

``KatParser`` Methods
=====================

.. rubric:: Parsing and building a model from a Kat file

.. autosummary::
    :toctree: generated/

    KatParser.__init__
    KatParser.parse
    KatParser.build
    KatParser.reset
