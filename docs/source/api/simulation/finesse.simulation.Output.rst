==============================================
``finesse.simulation.Output`` --- Output class
==============================================

.. currentmodule:: finesse.simulation

Overview
========

.. autoclass:: Output

Properties
==========

.. autosummary::
    :toctree: generated/

    Output.axes
    Output.entries
    Output.outputs
    Output.shape

Methods
=======

.. autosummary::
    :toctree: generated/

    Output.__init__
    Output.expand
    Output.update