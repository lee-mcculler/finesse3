===============================================================
``finesse.simulation.BaseSimulation`` --- Base simulation class
===============================================================

.. currentmodule:: finesse.simulation

Overview
========

.. autoclass:: BaseSimulation

Properties
==========

.. autosummary::
    :toctree: generated/

    BaseSimulation.frequencies
    BaseSimulation.M
    BaseSimulation.model
    BaseSimulation.name
    BaseSimulation.nodes
    BaseSimulation.num_equations

Methods
=======

.. rubric:: Indices

.. autosummary::
    :toctree: generated/

    BaseSimulation.findex
    BaseSimulation.field

.. rubric:: Executing

.. autosummary::
    :toctree: generated/

    BaseSimulation.run
