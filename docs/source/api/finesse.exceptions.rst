========================================
``finesse.exceptions`` --- Custom errors
========================================

.. automodule:: finesse.exceptions
.. currentmodule:: finesse.exceptions

Classes
=======

.. autoclass:: NodeException
.. autoclass:: TotalReflectionError

--------

``NodeException`` Properties
============================

.. autosummary::
    :toctree: generated/

    NodeException.node

``NodeException`` Methods
=========================

.. rubric:: Constructing a node exception

.. autosummary::
    :toctree: generated/

    NodeException.__init__

--------

``TotalReflectionError`` Properties
===================================

.. autosummary::
    :toctree: generated/

    TotalReflectionError.coupling

``TotalReflectionError`` Methods
================================

.. rubric:: Constructing a total reflection error instance

.. autosummary::
    :toctree: generated/

    TotalReflectionError.__init__
