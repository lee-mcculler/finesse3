====================================================
``finesse.utilities.maths`` -- Mathematical routines
====================================================

.. automodule:: finesse.utilities.maths

.. currentmodule:: finesse.utilities.maths

Functions
=========

.. rubric:: Complex numbers

.. autosummary::
    :toctree: generated/

    z_by_ph

.. rubric:: Higher Order Modes

.. autosummary::
    :toctree: generated/

    apply_ABCD
