.. _changes:
.. include:: /defs.hrst

Changes compared to |Finesse| 2
===============================

The following collects proposed conceptual changes between |Finesse| 2 and 3. Some of these are 
simple related to the underlying code rather than any functionality seen by a user.

Detectors
---------

|Finesse| 2 provides a number of so-called detectors, such as ``ad``, ``pd``, ``ccd``, etc. In 
addition some other commands will provide output like a detector, for example, ``lock`` and ``func``. 

.. Todo: maybe add links to detectors names in text above?

To prepare |Finesse| for additional features, we want to introduce a more formal separation between the
generation of output signal and the 'detection' of signals.  |Finesse| 3 distinguishes between 'probes',
which generate the output (similar to the commands mentioned above) and 'detectors' which are
now components, similar to a mirror. See :ref:`detectors_probes` for a description of the
concepts used in |Finesse| 3. 