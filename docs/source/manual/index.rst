.. _manual:

Manual
======

The manual contains a complete decription of the physics behind and the usage
of Finesse from an end-user's point of view. It also serves as a reference
point for various functions and components to link to.

.. toctree::
    :maxdepth: 1

    mathematical_description/index.rst
    higher_order_modes/index.rst
