# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals
import pytest

from finesse.testing import relfile_test

def pytest_addoption(parser):
    parser.addoption(
        "--plot",
        action="store_true",
        dest = 'plot',
        help = "Have tests update plots (it is slow)",
    )

    parser.addoption(
        "--do-benchmarks",
        action = "store_true",
        help   = "run slow benchmarking tests"
    )

    parser.addoption(
        "--do-stresstest",
        action = "store_true",
        help   = "Run slow repeated stress tests"
    )

@pytest.fixture
def plot(request):
    return request.config.getvalue('plot')
    return request.config.option.plot

@pytest.fixture()
def tpath(request):
    if isinstance(request.node, pytest.Function):
        return relfile_test(request.node.function.__code__.co_filename, request, 'data')
    raise RuntimeError("TPath currently only works for functions")

@pytest.fixture()
def ic():
    """
    Fixture to provide icecream imports without requiring that the package exist.
    Falls back to ipython's pretty printer or pythons if needed.

    https://github.com/gruns/icecream (on PyPI/pip as well)
    """
    try:
        from icecream import ic
        return ic
    except ImportError:
        pass
    try:
        from IPython.lib.pretty import pprint
        return pprint
    except ImportError:
        from pprint import pprint
        return pprint

