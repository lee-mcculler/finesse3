import matplotlib.pyplot as plt
from finesse import Model
from finesse.components import Beamsplitter, Laser
from finesse.gaussian import BeamParam
import pykat
import pykat.optics.gaussian_beams as gb

def comparison_plot(zs, ws_f, gs_f, ws_p, gs_p):
    fig, axes = plt.subplots(2, 2, sharex=True)
    axes[0, 0].plot(zs, ws_f, linestyle='-', color='r',
                    label=r"Finesse 3 trace")
    axes[0, 0].plot(zs, ws_p, linestyle='--', color='r',
                    label=r"Pykat trace")
    axes[1, 0].plot(zs, gs_f, linestyle='-', color='b',
                    label=r"Finesse 3 trace")
    axes[1, 0].plot(zs, gs_p, linestyle='--', color='b',
                    label=r"Pykat trace")

    axes[0, 1].plot(zs, (ws_f - ws_p)/ws_p, color='k')
    axes[0, 1].set_ylabel("Beamsize relative error")
    axes[1, 1].plot(zs, (gs_f - gs_p)/gs_p, color='k')
    axes[1, 1].set_ylabel("Gouy phase relative error")

    axes[0, 0].legend()
    axes[1, 0].legend()
    axes[0, 0].set_ylabel("Beamsize [mm]")
    axes[1, 0].set_xlabel("Distance z [m]")
    axes[1, 1].set_xlabel("Distance z [m]")
    axes[1, 0].set_ylabel("Gouy phase [deg]")
    fig.tight_layout()
    return fig, axes

def finesse_triangular():
    ifo = Model()
    lsr = Laser('lsr')
    m1 = Beamsplitter('m1', alpha=44.59)
    m2 = Beamsplitter('m2', Rc=27.24, alpha=0.82)
    m3 = Beamsplitter('m3', alpha=44.59)
    ifo.add([lsr, m1, m2, m3])
    ifo.connect(lsr.p1, m1.p1, L=1.0)
    ifo.connect(m1.p2, m2.p1, L=16.24057)
    ifo.connect(m2.p2, m3.p1, L=16.24057)
    ifo.connect(m3.p2, m1.p4, L=0.465)
    return ifo

def pykat_triangular():
    base = pykat.finesse.kat()
    base.verbose = False
    base.noxaxis = True
    base.parse("""
    l lsr 1 0 n0
    s s1 1.0 n0 nm1_in

    bs m1 0.5 0.5 0 44.59 nm1_in nm1_refl nm1_trns nm1_from_nm3
    s sc1 16.24057 nm1_trns nm2_in
    bs m2 0.5 0.5 0 0.82 nm2_in nm2_refl nm2_trns dump
    s sc2 16.24057 nm2_refl nm3_in
    bs m3 0.5 0.5 0 44.59 nm3_in nm3_refl nm3_trns nm3_return_refl
    s sc3 0.465 nm3_refl nm1_from_nm3

    attr m2 Rc 27.24
    """)
    return base

ft = finesse_triangular()
q_in = BeamParam(w0=5e-4, z=-11.0)
trace_f = ft.beam_trace_path(
    q_in=q_in,
    from_node=ft.lsr.p1.o,
    to_node=ft.m1.p4.i
)[0]
zs_f, ws_f, gs_f = trace_f.plot(show=False, filename="finesse3_trace_triangular.png")

pt = pykat_triangular()
q_in_p = gb.BeamParam(w0=5e-4, z=-11.0)
zs_p, ws_p, gs_p = pt.beamTrace(q_in_p, 'n0', 'nm3_in').plot(filename="pykat_trace_triangular.png")

fig = comparison_plot(zs_f, ws_f, gs_f, ws_p, gs_p)[0]
fig.savefig("pykat_finesse_triangular_path_trace.png")
