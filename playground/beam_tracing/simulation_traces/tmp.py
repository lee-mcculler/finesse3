import cProfile
import pstats

import finesse
from finesse.utilities.xaxis import xaxis, noxaxis

#finesse.LOGGER.setLevel("INFO")

ifo = finesse.parse("""
l L0 1 0 n0

s s1 0 n0 nEOM1
mod EOM1 1M .1 1 pm nEOM1 nEOM2
s s2 1 nEOM2 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ITM Rc -10
attr ETM Rc 10

attr ITM ybeta 1u

maxtem 6

cav FP ITM nITM2 ETM nETM1
gauss* gL0 L0 n0 -1.6 2.0

ad R 0 nITM1
ad C 0 nETM1
ad T 0 nETM2

ad Cp1 $EOM1.f nETM1
ad Cm1 $-1*EOM1.f nETM1

ad Cp1_01 0 1 $EOM1.f nETM1
""")

#cProfile.run("xaxis(ifo.ITM.phi, -200, 180, 500)", "fp_misaligned")
#p = pstats.Stats("fp_misaligned")
#p.sort_stats("time")
#p.print_stats(25)

out = xaxis(ifo.ITM.phi, -200, 180, 400)
#out = noxaxis(ifo)

#import matplotlib.pyplot as plt
#
#plt.plot(out.x1, abs(out['Cp1']), label="HG00 upper sideband")
#plt.plot(out.x1, abs(out['Cp1_01']), label="HG01 upper sideband")
#plt.legend()
#plt.show()
