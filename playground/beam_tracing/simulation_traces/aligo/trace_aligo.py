import cProfile
import pstats

from finesse.parse import KatParser
#import pykat
#from pykat.tools.detecting import all_bp_detectors

k = KatParser()
with open("../../../aligo/design.kat", "r") as file:
    katfile = file.read()

k.parse(katfile)
ifo = k.build()
cProfile.run("ifo.beam_trace()", "aligo_bt_prof")
p = pstats.Stats("aligo_bt_prof")
p.sort_stats("time")
p.print_stats(25)

#ifo.beam_trace()



#base = pykat.finesse.kat("../../../aligo/design.kat")
#base.parse(all_bp_detectors(base))
#base.parse("""
#noxaxis
#yaxis re:im
#""")
#out = base.run()
#print("\nFinesse 2 trace results:")
#print("--------------------------")
#for probe in base.detectors.keys():
#    print(probe[2:] + " qx = {}".format(out[probe]))
