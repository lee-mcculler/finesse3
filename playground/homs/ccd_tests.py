import matplotlib.pyplot as plt
import finesse

from finesse.detectors import CCD

CODE = """
l L0 1 0 n0

s s2 0 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ITM Rc -10
attr ETM Rc 10

maxtem 0

#gauss* gL0 L0 n0 -1 2
cav FP ITM nITM2 ETM nETM1

noxaxis
"""

ifo = finesse.parse(CODE)
ifo.add(CCD("ccd", ifo.n0, [-5, 5], [-5, 5], 100, f=0))

out = ifo.run()
#print(out["ccd"])


fig, ax = plt.subplots()
surf = ax.imshow(
    abs(out["ccd"]),
    extent=[ifo.ccd.x.min(), ifo.ccd.x.max(),
            ifo.ccd.y.min(), ifo.ccd.y.max()],
    origin='lower',
    #vmin=0, vmax=np.abs(Z).max()
)
ax.axis(aspect='image')
ax.set_xlabel('$x$ [$w_0$]')
ax.set_ylabel('$y$ [$w_0$]')
ax.grid(False)
plt.show()
