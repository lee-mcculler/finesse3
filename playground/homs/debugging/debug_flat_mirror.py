#import matplotlib.pyplot as plt

CODE = """
l L0 1 0 n0
s s1 0 n0 nITM1
m ITM 0.99 0.01 0 nITM1 nITM2

maxtem 2
gauss gL0 L0 n0 1e-2 10

attr ITM xbeta 1u

ad ad00 0 0 0 nITM1
ad ad10 1 0 0 nITM1
noxaxis
"""

def finesse2():
    import pykat

    model = pykat.finesse.kat()
    model.verbose = False
    model.parse(CODE)
    model.parse("trace 40")
    out = model.run()
    print(out.stdout)


def finesse3():
    import finesse
    from finesse.utilities.xaxis import xaxis, noxaxis

    finesse.LOGGER.setLevel("INFO")

    ifo = finesse.parse(CODE)
    ifo.ITM.select_loggable_knm_matrices("K22")#, couplings=[(0,0,1,0), (1,0,0,0)])

    out = ifo.run()
    return out

finesse2()
finesse3()
