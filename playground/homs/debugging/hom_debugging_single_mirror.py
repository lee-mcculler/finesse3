from unittest import TestCase

import matplotlib.pyplot as plt

CODE = """
l L0 1 0 n0
s s1 0 n0 nITM1
m ITM 0.99 0.01 0 nITM1 nITM2

maxtem 4
gauss gL0 L0 n0 1e-3 -2

attr ITM xbeta 1u

ad ad00 0 0 0 nITM1
ad ad10 1 0 0 nITM1
ad ad20 2 0 0 nITM1
ad ad30 3 0 0 nITM1
ad ad40 4 0 0 nITM1

xaxis ITM xbeta lin 1u 100u 100
yaxis re:im
"""

def finesse2():
    import pykat

    model = pykat.finesse.kat()
    model.verbose = False
    model.parse(CODE)
    #model.parse("""
    ##yaxis re:im
    ##trace 40
    ##debug 128
    #""")
    out = model.run()
    #print("Finesse 2:")
    #print(f"ad00 = {out['ad00']}")
    #print(f"ad10 = {out['ad10']}")
    return out

def finesse3():
    import finesse
    #from finesse.utilities.xaxis import xaxis, noxaxis

    #finesse.LOGGER.setLevel("INFO")

    ifo = finesse.parse(CODE)
    #ifo.ITM.select_loggable_knm_matrices("K11")#, couplings=[(0,0,1,0), (1,0,0,0)])
    #ifo.elements['s1'].select_loggable_knm_matrices([])

    out = ifo.run()
    #print("Finesse 3:")
    #print(f"ad00 = {out['ad00']}")
    #print(f"ad10 = {out['ad10']}")
    return out

outf2 = finesse2()
outf3 = finesse3()


def rel_err(x, y):
    return abs((x - y)/y)

fig, ax = plt.subplots(2, 1, sharex=True)
cols = ['xkcd:blue', 'xkcd:red', 'xkcd:orange', 'xkcd:darkgreen', 'xkcd:purple']
for i in range(4):
    ax[0].semilogy(outf3.x1, abs(outf2[f"ad{i}0"]), color=cols[i], label=f"HG{i}0 Finesse 2")
    ax[0].semilogy(outf3.x1, abs(outf3[f"ad{i}0"]), color=cols[i], linestyle="--", label=f"HG{i}0 Finesse 3")
    ax[1].plot(outf3.x1, rel_err(abs(outf2[f"ad{i}0"]), abs(outf3[f"ad{i}0"])), color=cols[i], label=f"HG{i}0 error")

ax[0].set_ylabel("amplitude")
ax[1].set_ylabel("relative error")
ax[1].set_xlabel("ITM xbeta [rad]")
ax[0].legend()
ax[1].legend()

fig.savefig("homdebug_sm.png", bbox_inches="tight")
