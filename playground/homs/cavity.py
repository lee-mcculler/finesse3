import numpy as np
import matplotlib.pyplot as plt

import finesse

import finesse.components as fc
import finesse.detectors as fd

from finesse.utilities.xaxis import xaxis

#finesse.LOGGER.setLevel("INFO")

model = finesse.Model(maxtem=10)

model.chain(
    fc.Laser("L0"), 0.5,
    fc.Mirror("ITM", Rc=-2.5, R=0.99, T=0.01), 1.0,
    fc.Mirror("ETM", Rc=2.5, R=0.99, T=0.01, xbeta=1e-5)
)
model.add(fc.Cavity("FP", model.ITM.p2.o, model.ITM.p2.i))

model.L0.p1.o.q = -1 + 0.8j

model.tag_node(model.ETM.p2.o, "nTRNS")
model.tag_node(model.ITM.p1.o, "nREFL")

model.add(fd.AmplitudeDetector("ad00_TRNS", model.nTRNS, 0))
model.add(fd.AmplitudeDetector("ad01_TRNS", model.nTRNS, 0, 0, 1))
model.add(fd.AmplitudeDetector("ad10_TRNS", model.nTRNS, 0, 1, 0))
model.add(fd.PowerDetector("pTRNS", model.nTRNS))

out = xaxis(model.ITM.p2.space.L, 0.8, 1.2, 100)

fig, ax = plt.subplots()
ax.plot(out.x1, np.abs(out["ad10_TRNS"])**2, label="HG10 transmission")
ax.set_xlabel("Cavity length [m]")
ax.set_ylabel("Power [W]")
ax.legend()

#plt.show()
#print(out["ad00_TRNS"])
