from finesse import Model, BeamParam
from finesse.components import Cavity, Laser, Mirror
from finesse.tracing.assembly import Assembly

model = Model()

model.chain(
    Laser('lsr'),
    0.5,
    Mirror('itm', Rc=-2.5),
    1.0,
    Mirror('etm', Rc=2.5)
)

model.add(Cavity('fpc', model.itm.n2.o, model.itm.n2.i))

model.lsr.n1.o.q = BeamParam(q=(-1.0 + 0.8j))

model.beam_trace()
print("Prior to solving:")
print(f"Beam size at ETM n1.o = {model.etm.n1.o.q.beamsize()} m")
print(f"Length of cavity = {model.itm.n2.o.space.L} m")
print(f"ITM RoC = {model.itm.Rc} m")

system = Assembly(model)
system.allow(model.itm.n2.o.space, 'L', bounds=(0, 10))
system.allow(model.itm, "Rc", bounds=(-2, 5))
system.target(model.etm.n1.o, 'q', "beamsize", value=5e-4)

result = system.solve(tol=1e-8)

print("\nAfter solving:")
print(f"Beam size at ETM n1.o = {model.etm.n1.o.q.beamsize()} m")
print(f"Length of cavity = {model.itm.n2.o.space.L} m")
print(f"ITM RoC = {model.itm.Rc} m")
