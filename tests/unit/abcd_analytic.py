"""Analytical forms of component ABCD matrices for use in test_abcd.py"""

import numpy as np

def abcd_space(L, nr):
    return np.array(
        [
            [1.0, L/nr],
            [0.0, 1.0]
        ]
    )

def abcd_lens(f):
    return np.array(
        [
            [1.0, 0.0],
            [-1.0/f, 1.0]
        ]
    )

def abcd_mirror_t(nr1, nr2, rc):
    return np.array(
        [
            [1.0, 0.0],
            [(nr2 - nr1)/rc, 1.0]
        ]
    )

def abcd_mirror_r(nr1, rc):
    return np.array(
        [
            [1.0, 0.0],
            [-2*nr1/rc, 1.0]
        ]
    )

def abcd_beamsplitter_tt(nr1, nr2, alpha1, alpha2, rc):
    dn = (nr2*np.cos(alpha2) - nr1*np.cos(alpha1))/(np.cos(alpha1)*np.cos(alpha2))
    return np.array(
        [
            [np.cos(alpha2)/np.cos(alpha1), 0.0],
            [dn/rc, np.cos(alpha1)/np.cos(alpha2)]
        ]
    )

def abcd_beamsplitter_ts(nr1, nr2, alpha1, alpha2, rc):
    dn = (nr2*np.cos(alpha2) - nr1*np.cos(alpha1))/(np.cos(alpha1)*np.cos(alpha2))
    return np.array(
        [
            [1.0, 0.0],
            [dn/rc, 1.0]
        ]
    )

def abcd_beamsplitter_rt(nr1, alpha1, rc):
    return np.array(
        [
            [1.0, 0.0],
            [-2*nr1/(rc*np.cos(alpha1)), 1.0]
        ]
    )

def abcd_beamsplitter_rs(nr1, alpha1, rc):
    return np.array(
        [
            [1.0, 0.0],
            [-2*nr1*np.cos(alpha1)/rc, 1.0]
        ]
    )
