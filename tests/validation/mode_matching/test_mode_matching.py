"""Mode matching optimiser tests."""

from unittest import TestCase
from numpy.testing import assert_allclose
from finesse import Model, BeamParam
from finesse.components import Laser, Lens, Mirror
from finesse.tracing.assembly import Assembly


class TelescopeTestCase(TestCase):
    """Two lens tests."""
    def setUp(self):
        self.model = Model()

        self.model.chain(
            Laser('lsr'),
            1.0,
            Lens('l1'),
            1.0,
            Lens('l2'),
            1.0,
            Mirror('m1'),
            1.0,
            Mirror('m2')
        )

        # Create system for optimiser.
        self.system = Assembly(self.model)

    def test_telescope_position_optimisation(self):
        """Test target beam size optimisation with two lenses of equal and opposite focal lengths."""
        initial_beam_size = 0.5e-3
        final_beam_size = 1.5e-3

        # Set beam waist to be at the laser.
        self.model.lsr.p1.o.q = BeamParam(w0=initial_beam_size, z=0)

        # Set lens focal lengths.
        self.model.l1.f = -1
        self.model.l2.f = 1

        # Set parameters that can be varied.
        self.system.allow(self.model.lsr.p1.o.space, 'L', bounds=(0.1, 1.5))
        self.system.allow(self.model.l1.p2.o.space, 'L', bounds=(0.1, 1.5))

        # Set target beam size at mirrors beyond lenses.
        self.system.target(self.model.m1.p1.i, 'qx', 'w', value=final_beam_size)
        self.system.target(self.model.m2.p1.i, 'qx', 'w', value=final_beam_size)

        # Solve.
        result = self.system.solve(tol=1e-10)

        # Check final beam is collimated.
        assert_allclose(self.model.m1.p1.i.qx.w, final_beam_size)
        assert_allclose(self.model.m2.p1.i.qx.w, final_beam_size)


if __name__ == '__main__':
    import unittest
    unittest.main()

