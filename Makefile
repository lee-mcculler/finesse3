default:
	python3 setup.py build_ext --build-lib .
	
clean:
	find . -name "*.so" -type f -delete
	find . -name "*.dll" -type f -delete

realclean: clean
	rm -rf `grep -o '^[^#]*' .gitignore`
	
lint:
	pylint --rcfile=.pylintrc ./finesse/
