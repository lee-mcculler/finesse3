"""
Sub-module consisting of the :class:`.Simulation` for
performing specific executions of a :class:`.Model`.
"""

import logging
import numpy as np
import re
import networkx as nx
import copy

from finesse.components import FrequencyGenerator, Space
from finesse.components.node import NodeType
from collections import defaultdict, OrderedDict

from . import base
from .matrix_digraph import (
    SRE_copy,
    SREdense_numeric_inverse,
    SREDenseInverter,
)

LOGGER = logging.getLogger(__name__)


def abs_sq(arr):
    return arr.real**2 + arr.imag**2


#@canFreeze
class DigraphSimulationBase(base.BaseSimulation):
    """
    Simulation class for executing configuration models.

    The `Simulation` class uses the :class:`.Model` to build
    and fill the sparse matrix and is used as a reference for
    detectors to compute outputs to return to the user.

    .. note::
        Instances of this class **cannot** be copied, they are generated
        from and operate on a :class:`.Model` object in a particular
        built state.

    .. note::
        The module :mod:`pickle` **cannot** be used on instances of this class.
    """
    _submatrix_semantic_map = None
    def print_matrix(self):
        raise NotImplementedError()

    def active(self):
        return self._submatrix_semantic_map is not None

    def set_source(self, field_idx, vector):
        if len(field_idx) == 3:
            node, freq_idx, subidx = field_idx
        elif len(field_idx) == 2:
            node, freq_idx = field_idx
            subidx = None

        freq = self._frequencies[freq_idx]
        Mnode = (node, freq)

        if subidx is None:
            assert(vector.shape == (self._M_sizes[Mnode],))
            self._M_sources[Mnode] = vector
        else:
            try:
                source_vect = self._M_sources[Mnode]
            except KeyError:
                source_vect = np.zeros((self._M_sizes[Mnode],), dtype = complex)
                self._M_sources[Mnode] = source_vect
            source_vect[subidx] = vector
        return

    def __getitem__(self, key):
        """
        Returns a dictionary of all the submatrices an element has requested
        for different connections it needs. The key is:

            (element, connection_name, ifreq, ofreq)

        element : finesse.component.Connector
            Is the object reference that created the requests
        connection_name : str
            String name given to the connection
        ifreq : finesse.Frequency
            Incoming frequency
        ofreq : finesse.Frequency
            Outgoing frequency

        Returns
        -------
        dict
        """
        semantic_dict = self._submatrix_semantic_map[key]
        Mnode_fr, Mnode_to = semantic_dict['iodx_semantic']
        is_diagonal = semantic_dict['is_diagonal']
        if Mnode_to in self._M_seq[Mnode_fr]:
            return self._M_edges[Mnode_fr, Mnode_to]
        else:
            self._M_seq[Mnode_fr].add(Mnode_to)
            self._M_req[Mnode_to].add(Mnode_fr)
            if not is_diagonal:
                shape = (self._M_sizes[Mnode_to], self._M_sizes[Mnode_fr])
                matrix = np.empty(shape, dtype = complex)
            else:
                shape = (self._M_sizes[Mnode_to],)
                assert(self._M_sizes[Mnode_to] == self._M_sizes[Mnode_fr])
                #builds only the diagonal. The matrix simplifier is responsible
                #for applying the correct matrix mul rules between
                #"diagonal matrices" (which are 1d arrays), scalars, and 2d matrices
                matrix = np.empty(shape, dtype = complex)
            self._M_edges[Mnode_fr, Mnode_to] = matrix
            #assumes that the getter will be filling this matrix
            return matrix

    def __enter__(self):
        """
        When entered the Simulation object will create the matrix to be used in
        the simulation.

        This is where the matrix gets allocated and constructed. It will expect
        that the model structure does not change after this until it has been
        exited.
        """

        self._M_sources              = dict()
        self._M_sizes                = dict()
        self._M_seq                  = defaultdict(set)
        self._M_req                  = defaultdict(set)
        self._M_edges                = dict()
        self._submatrix_semantic_map = OrderedDict()

        self._initialise()

        self._fill()

        # TODO ddb: this should be chcking audio and that we have quantum calculations going on
        # TODO ddb: create sparse noise source matrix
        # TODO ddb: noise calculations probably more generic than just quantum
        return self

    def _initialise_submatrices(self):
        for n, node_inf in self._node_info.items():
            # Optical nodes have HOM information, mech/elec don't, all have frequencies though
            if n.type is NodeType.OPTICAL:
                Nsm = node_inf.nfreqs
                #Neq = node_inf.nhoms
                for fidx in range(Nsm):
                    freq = self._frequencies[fidx]
                    Mnode_key = (n, freq)
                    self._M_seq[Mnode_key].add(Mnode_key)
                    self._M_req[Mnode_key].add(Mnode_key)
                    self._M_edges[Mnode_key, Mnode_key] = 1
                    self._M_sizes[Mnode_key] = node_inf.nhoms

            elif n.type is NodeType.MECHANICAL or n.type is NodeType.ELECTRICAL:
                freq = self._frequencies[0]
                Mnode_key = (n, freq)
                self._M_seq[Mnode_key].add(Mnode_key)
                self._M_req[Mnode_key].add(Mnode_key)
                self._M_edges[Mnode_key, Mnode_key] = 1
                self._M_sizes[Mnode_key] = 1
            else:
                raise Exception("Not handled")

        # Loop over every edge in the network which represents a bunch of
        # connections between frequencies and HOMs between two nodes
        _done = {}
        for owner in self._edge_owners:
            if owner in _done:
                continue

            couples_f = isinstance(owner, FrequencyGenerator)

            # For each connection this element wants...
            for name in owner._registered_connections:
                nio = tuple((owner.nodes[_] for _ in owner._registered_connections[name]))  # convert weak ref (input, output)
                # If we are a carrier matrix only compute optics, no AC
                if not self.is_audio:
                    if (nio[0].type is not NodeType.OPTICAL or nio[1].type is not NodeType.OPTICAL):
                        continue

                # Loop over all the frequencies we can couple between and add
                # submatrixes to the overall model
                for i, ifreq in enumerate(self.frequencies):
                    for j, ofreq in enumerate(self.frequencies):
                        #for k in range(Nhom):
                        # For each input and output frequency check if our
                        # element wants to couple them at this
                        if couples_f and not owner._couples_frequency(self, name, ifreq, ofreq):
                            continue
                        elif not couples_f and ifreq != ofreq:
                            # If it doesn't couple frequencies and the
                            # frequencies are different then ignore
                            continue

                        iodx_semantic = []  # submatrix indices
                        iodx = []  # submatrix indices
                        tags = []  # descriptive naming tags for submatrix key
                        key_name = re.sub(r"^[^.]*\.", "", name)
                        key_name = re.sub(r">[^.]*\.", ">", key_name)
                        key  = [owner, key_name]

                        # Get simulation unique indices for submatrix
                        # position. How we get these depends on the type of
                        # the nodes involved
                        for freq, node in zip((ifreq, ofreq), nio):
                            if node.type is NodeType.OPTICAL:
                                iodx_semantic.append((node, freq))
                                iodx.append(self.findex(node, freq.index))
                                tags.append(freq.name)
                                key.append(freq)
                            else:
                                # Mechanical and electrical don't have multiple
                                # freqs, so always use the zeroth frequency index
                                iodx_semantic.append((node, self._frequencies[0]))
                                iodx.append(self.findex(node, self._frequencies[0]))
                                tags.append('audio')
                                key.append(None)

                        assert(len(iodx) == 2)
                        assert(len(key) == 4)

                        #TODO, there should be a better registry for 
                        #diagonals support
                        if isinstance(owner, Space):
                            is_diagonal = True
                        else:
                            is_diagonal = False

                        if tuple(key) not in self._submatrix_semantic_map:
                            self._submatrix_semantic_map[tuple(key)] = dict(
                                iodx_semantic = iodx_semantic,
                                is_diagonal = is_diagonal,
                            )
            _done[owner] = True
        return

    def __exit__(self, type_, value, traceback):
        self._unbuild()
        self._submatrix_semantic_map  = None

    _first_solve = True
    def solve(self):
        if not nx.is_frozen(self.model.network):
            raise Exception("Model has not been built")

        SRE = SRE_copy((
            self._M_seq,
            self._M_req,
            self._M_edges,
        ))
        seq, req, edges = SRE

        #create a principle node to propagate the sources from
        Mrhs_prime = 'rhs'
        seq[Mrhs_prime].add(Mrhs_prime)
        req[Mrhs_prime].add(Mrhs_prime)
        edges[Mrhs_prime, Mrhs_prime] = 1

        for Mnode_src, src_vec in self._M_sources.items():
            seq[Mrhs_prime].add(Mnode_src)
            req[Mnode_src].add(Mrhs_prime)
            edges[Mrhs_prime, Mnode_src] = src_vec.reshape(-1, 1)

        #if self._first_solve:
        #    from .matrix.SRE_graphs import SRE_graph
        #    SRE_graph('test_network.pdf', SRE)
        #solve into out_semantic, but also create the usual "out" indexed vector
        #so that output requests elsewhere in the code can work

        SREinv = SREdense_numeric_inverse(
            seq,
            req,
            edges,
            inputs = [Mrhs_prime],
            outputs = seq.keys(),
        )

        seq_i, req_i, edges_i = SREinv
        out_semantic = dict()
        for Mnode in seq_i[Mrhs_prime]:
            edges_i[Mrhs_prime, Mnode]
            out_semantic[Mnode] = edges_i[Mrhs_prime, Mnode]

        self._first_solve = False
        self.out_semantic = out_semantic

        #TODO, return the correct output type
        self.out = out_semantic
        return

    def _clear_rhs(self):
        self._M_sources.clear()
        return

    def get_DC_out(self, node, freq=0, hom=0):
        Mnode = (node, self._frequencies[freq])
        outvec = self.out_semantic[Mnode]
        return outvec[hom]


class DigraphSimulation(DigraphSimulationBase):
    _first_solve = True
    def solve(self):
        if not nx.is_frozen(self.model.network):
            raise Exception("Model has not been built")

        SRE = SRE_copy((
            self._M_seq,
            self._M_req,
            self._M_edges,
        ))
        seq, req, edges = SRE

        #create a principle node to propagate the sources from
        Mrhs_prime = 'rhs'
        seq[Mrhs_prime].add(Mrhs_prime)
        req[Mrhs_prime].add(Mrhs_prime)
        edges[Mrhs_prime, Mrhs_prime] = 1

        for Mnode_src, src_vec in self._M_sources.items():
            seq[Mrhs_prime].add(Mnode_src)
            req[Mnode_src].add(Mrhs_prime)
            edges[Mrhs_prime, Mnode_src] = src_vec.reshape(-1, 1)

        sizes = dict(self._M_sizes)
        sizes['rhs'] = 1
        SREinv = SREDenseInverter(
            seq,
            req,
            edges,
            sizes   = sizes,
            inputs  = [Mrhs_prime],
            outputs = seq.keys(),
        )
        #if self._first_solve:
        #    SREinv.plot_graph('test_network_0.pdf')
        #solve into out_semantic, but also create the usual "out" indexed vector 
        #so that output requests elsewhere in the code can work
        N = 0
        while SREinv.simplify_trivial():
            N += 1
            #if self._first_solve:
            #    SREinv.plot_graph('test_network_{}.pdf'.format(N))
        N += 1
        #if self._first_solve:
        #    SREinv.plot_graph('test_network_{}.pdf'.format(N))
        seq_i, req_i, edges_i = SREinv.numeric_finish()
        out_semantic = dict()
        for Mnode in seq_i[Mrhs_prime]:
            edges_i[Mrhs_prime, Mnode]
            out_semantic[Mnode] = edges_i[Mrhs_prime, Mnode]

        self._first_solve = False
        self.out_semantic = out_semantic

        #TODO, return the correct output type
        self.out = out_semantic


class DigraphSimulationReducing(DigraphSimulationBase):
    _first_solve = True
    _last_edges = None
    _presolve = None
    def solve(self):
        if not nx.is_frozen(self.model.network):
            raise Exception("Model has not been built")

        SRE = SRE_copy((
            self._M_seq,
            self._M_req,
            self._M_edges,
        ))
        seq, req, edges = SRE

        #create a principle node to propagate the sources from
        Mrhs_prime = 'rhs'
        seq[Mrhs_prime].add(Mrhs_prime)
        req[Mrhs_prime].add(Mrhs_prime)
        edges[Mrhs_prime, Mrhs_prime] = 1

        for Mnode_src, src_vec in self._M_sources.items():
            seq[Mrhs_prime].add(Mnode_src)
            req[Mnode_src].add(Mrhs_prime)
            edges[Mrhs_prime, Mnode_src] = src_vec.reshape(-1, 1)

        #fill has just happened, so determine changing edges
        edges_changed = dict()
        if self._last_edges is not None:
            for ekey, edge in edges.items():
                edge_last = self._last_edges[ekey]
                if np.any(abs_sq(edge - edge_last) > 1e-12):
                    edges_changed[ekey] = edge

        #because the fill is in-place, one must copy all of the matrices,
        #slowslow
        last_edges = dict()
        for ekey, Medge in edges.items():
            last_edges[ekey] = np.copy(Medge)

        self._last_edges = last_edges

        if self._presolve is not None:
            if set(self._presolve.edges_changing.keys()) != set(edges_changed.keys()):
                print("PRESOLVING")
                #print(self._presolve.edges_changing.keys())
                #print(edges_changed.keys())
                self._presolve = None

        #print(edges_changed.keys())

        if self._presolve is None:
            #if self._first_solve:
            #    from .matrix.SRE_graphs import SRE_graph
            #    SRE_graph('test_network.pdf', SRE)
            #solve into out_semantic, but also create the usual "out" indexed vector
            #so that output requests elsewhere in the code can work

            sizes = dict(self._M_sizes)
            sizes['rhs'] = 1
            SREinv = SREDenseInverter(
                seq,
                req,
                dict(edges),
                sizes   = sizes,
                inputs  = [Mrhs_prime],
                outputs = seq.keys(),
            )
            SREinv.set_changing_edges(edges_changed)

            while SREinv.simplify_trivial():
                pass

            self._presolve = SREinv

        SREinv = copy.copy(self._presolve)
        SREinv.reinject_changed_edges(edges_changed)
        while SREinv.simplify_trivial():
            pass

        seq_i, req_i, edges_i = SREinv.numeric_finish()
        out_semantic = dict()
        for Mnode in seq_i[Mrhs_prime]:
            edges_i[Mrhs_prime, Mnode]
            out_semantic[Mnode] = edges_i[Mrhs_prime, Mnode]

        self._first_solve = False
        self.out_semantic = out_semantic
        
        #TODO, return the correct output type
        self.out = out_semantic
        return
