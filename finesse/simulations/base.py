"""
Sub-module consisting of the :class:`.Simulation` for
performing specific executions of a :class:`.Model`.
"""

import logging
import weakref
import numpy as np
import re
import networkx as nx
import sys
import gc

from finesse.cmatrix import _Column, _SubMatrix
from numpy.lib.stride_tricks import as_strided

from collections import OrderedDict, namedtuple
from finesse.cmatrix import KLUMatrix
from finesse.components import FrequencyGenerator, Space
from finesse.components.node import NodeType
from finesse.freeze import canFreeze
from finesse.frequency import Frequency, generate_frequency_list
from finesse.enums import SpatialType
from finesse.utilities import check_name
from finesse.element import Symbol
from copy import deepcopy

LOGGER = logging.getLogger(__name__)


@canFreeze
class BaseSimulation(object):
    """
    Base Simulation class for executing a model.

    The `Simulation` class uses the :class:`.Model` to build
    and fill the sparse matrix and is used as a reference for
    detectors to compute outputs to return to the user.

    .. note::
        Instances of this class **cannot** be copied, they are generated
        from and operate on a :class:`.Model` object in a particular
        built state.

    .. note::
        The module :mod:`pickle` **cannot** be used on instances of this class.
    """
    def __init__(self, model, name, frequencies=None, is_audio=False):
        self._model       = model
        self._name        = name
        self._to_update   = []
        self._audio_freq  = bool(is_audio)
        self._input_frequencies = frequencies
        self._frequency_map = None

        # register that this simulation is dependant on this model
        model._register_simulation(self)

    @property
    def is_audio(self): return self._audio_freq

    @property
    def is_modal(self): return self.model.spatial_type == SpatialType.MODAL

    @property
    def frequency_map(self): return self._frequency_map

    @property
    def model(self):
        """A reference to the underlying :class:`.Model` instance.

        :getter: Returns a reference to the :class:`.Model` instance
                 used by this simulation (read-only).
        """
        return self._model

    @property
    def nodes(self):
        """The nodes of the underlying model.

        :getter: Returns a list of the simulation's nodes (read-only).
        """
        return self._nodes

    @property
    def homs(self):
        return self._HOMs

    @property
    def nhoms(self):
        return self._HOMs.shape[0]

    @property
    def frequencies(self):
        """The :class:`.Frequency` objects present in this simulation.

        :getter: Returns a list of the simulation's frequencies (read-only).
        """
        return self._frequencies

    @property
    def name(self):
        """Name of the simulation.

        :getter: Returns the name of the simulation (read-only).
        """
        return self._name

    @property
    def num_equations(self):
        r"""
        The number of equations the configuration consists of, given by,

        .. math::
            n_{\mathrm{eq}} = n_{\mathrm{nodes}} n_{\mathrm{freq}} n_{\mathrm{HOM}},

        where :math:`n_{\mathrm{nodes}}` is the number of nodes in the network,
        :math:`n_{\mathrm{freq}}` is the number of frequencies and
        :math:`n_{\mathrm{HOM}}` is the number of higher-order modes.

        :getter: Returns the numbers of equations describing the system (read-only).
        """
        return len(self.model.network.nodes) * len(self._frequencies) * self._HOMs.shape[0]

    def findex(self, node, freq):
        """
        Returns simulation unique index for a given frequency at this node.
        Used to refer to submatrices of HOMs in the interferometer matrix.

        Parameters
        ----------
        node : :class:`.Node`
            Node object to get the index of.
        freq : int
            Frequency index.

        Returns
        -------
        index : int
            Index of the `node` for a given frequency.
        """
        assert(freq < self._node_info[node].nfreqs) # Requested frequency not available for this node
        return self._node_info[node].freq_index + freq

    def field(self, node, freq=0, hom=0):
        """
        Returns simulation unique index of a field at a particular frequency
        index at this node.

        Parameters
        ----------
        node : :class:`.Node`
            Node object to get the index of.
        freq : int
            Frequency index.
        hom : int, optional
            Higher Order Mode index, defaults to zero.
        """
        Nf = self._node_info[node].nfreqs
        Nh = self._node_info[node].nhoms
        assert(freq < Nf)  # Requested frequency not available for this node
        assert(hom < Nh)   # Requested hom not available for this node
        return self._node_info[node].rhs_index + freq * Nh + hom

    def get_DC_out(self, node, freq=0, hom=0):
        return self.out[self.field(node, freq, hom)]

    def __getitem__(self, key):
        """
        Returns a dictionary of all the submatrices an element has requested
        for different connections it needs. The key is:

            (element, connection_name, ifreq, ofreq)

        element : finesse.component.Connector
            Is the object reference that created the requests
        connection_name : str
            String name given to the connection
        ifreq : finesse.Frequency
            Incoming frequency
        ofreq : finesse.Frequency
            Outgoing frequency

        Returns
        -------
        dict
        """
        return self._submatrices[key]

    def compute_knm(self):
        for fn in self._compute_knm_matrices:
            fn(self)

    def set_spaces_gouy_phase(self):
        for fn in self._set_gouy_phases:
            fn(self)

    def fill(self, fill_all=True, carrier=None):
        if fill_all:
            todo = self._edge_owners
        else:
            todo = self._to_update

        for owner in todo:
            owner._fill_matrix(self, carrier=carrier)

    def fill_rhs(self, carrier=None):
        for fn in self._fill_rhs:
            fn(self, carrier)

    def fill_qnoise_rhs(self):
        if self.is_modal:
            Nhom = self._HOMs.shape[0]
        else:
            Nhom = 1

        for comp in self._components:
            # Fill in all open port vacuum contributions
            for port in comp.ports:
                if port.type != NodeType.OPTICAL or port.is_connected:
                    continue
                for freq in self.frequencies:
                    for hom in range(Nhom):
                        idx = self.field(port.i, freq.index, hom)
                        self._Mq[idx] = self.model._UNIT_VACUUM / 2 * (1 + freq.carrier.f / self.model.f0)
            # Then ask components for their noise contributions
            if hasattr(comp, "_fill_qnoise_rhs"):
                comp._fill_qnoise_rhs(self)

    def get_frequency_object(self, frequency):
        """
        This returns a :class:`finesse.frequency.Frequency` object.

        Parameters
        ----------
        f : Number or :class:`finesse.element.Symbol`
            Frequency to search for in this simulation
        """
        if isinstance(frequency, Symbol):
            if frequency.is_changing:
                # if it's tunable we want to look for the symbol that is just this
                # lasers frequency, as it will be changing
                for f in self.frequencies:
                    if f.symbol == frequency:
                        return f

        f_value = float(frequency)
        # otherwise do some value comparisons
        for f in self.frequencies:
            if float(f.f) == f_value:
                return f

        return None

    def _initialise(self):
        self._HOMs         = self.model.homs.copy()
        self._components   = self.model.components
        self._detectors    = self.model.detectors
        self._submatrices  = OrderedDict()
        self._node_info    = OrderedDict()
        self.NodeInfoEntry = namedtuple("NodeInfoEntry", "index rhs_index freq_index nfreqs nhoms")

        # TODO: (sjr) where should beam tracing be called? Here seems most sensible
        #       place as _on_build methods may require use of beam params, but
        #       need a way to pass args to beam_trace call => some beam_trace_args
        #       field of Model perhaps
        if self.is_modal: self.model.beam_trace(**self.model.beam_trace_args)

        network = self.model.network

        if not self.is_audio:
            LOGGER.info("Building DC simulation")
            # For now we just get all the optical nodes to simulate in the DC matrix
            # convert from weakrefs
            self._nodes = {n: network.nodes[n]['weakref']()
                           for n in network.nodes
                           if 'optical' in network.nodes[n]}
        else:
            LOGGER.info("Building AC simulation")
            # For audio matrix we solve all types
            self._nodes = {n: network.nodes[n]['weakref']() for n in network.nodes}

        self._setup_frequencies()

        if self.is_modal:
            self._Nhom = self._HOMs.shape[0]
        else:
            self._Nhom = 1

        self._initialise_nodes()
        self._initialise_elements()
        self._initialise_submatrices()
        
        self.out = np.atleast_2d(np.zeros((self.num_equations,1), dtype=complex))

    def _setup_frequencies(self):
        # If no frequencies were specified then generate a default set
        if self._input_frequencies is None:
            self._input_frequencies = generate_frequency_list(self.model)

        self._frequencies = []

        LOGGER.info('Generating simulation with carrier frequencies %s', self._input_frequencies)
        # Now loop over the requested frequencies for this simulation
        # and generate the frequency bin objects
        if not self.is_audio:
            for i, f in enumerate(self._input_frequencies):
                self._frequencies.append(
                    Frequency(str(f.eval(keep_changing_symbols=True)), self, f, index=i)
                )
        else:
            for i, f in enumerate(self._input_frequencies):
                fp = f + self.model.fsig.f.ref
                fm = f - self.model.fsig.f.ref
                self._frequencies.append(
                    Frequency(str(fp.eval(keep_changing_symbols=True)),
                              self, fp, index=2*i, audio_order=1,
                              audio=True, audio_carrier_index=i)
                )
                self._frequencies.append(
                    Frequency(str(fm.eval(keep_changing_symbols=True)),
                              self, fm, index=2*i+1, audio_order=-1,
                              audio=True, audio_carrier_index=i)
                )

        self._Nf = len(self._frequencies)

    def _initialise_nodes(self):
        Nhom = self._Nhom
        Nf   = self._Nf

        assert(Nhom > 0)
        assert(Nf > 0)

        s_rhs_idx = 0
        s_f_idx = 0

        for i, n in enumerate(self.nodes.values()):
            if n.type is NodeType.OPTICAL:
                #(index, RHS index, frequency index, num freqs, num HOM)
                Nsm   = Nf  # Each frequency is a submatrix of HOMs
                Neq = Nhom  # Number of equations in each submatrix
            elif n.type is NodeType.MECHANICAL:
                # Mechanics only has single-sided audio frequency term
                # TODO ddb: I suppose you could also have HOM mechanical resonances such as PI here
                Nsm = 1
                Neq = 1
            elif n.type is NodeType.ELECTRICAL:
                Nsm = 1  # TODO ddb: add electronic frequencies, will need new frequency list in the models
                Neq = 1  # no higher order modes of electronics as far as I'm aware...
            else:
                raise Exception("Not handled")

            self._node_info[n] = self.NodeInfoEntry(i, s_rhs_idx, s_f_idx, Nsm, Neq)
            s_rhs_idx += Neq * Nsm  # Track how many equations we are going through
            s_f_idx   += Nsm  # keep track of how many frequencies*nodes
            #print(n, self._node_info[n])

    def _initialise_elements(self):
        # Store all the edge owners we'll need to loop over and call fill on
        self._edge_owners = []
        # use set for unique edges
        for el in set(nx.get_edge_attributes(self.model.network, "owner").values()):
            self._edge_owners.append(el())

        # Store all the elements that might fill the RHS
        self._fill_rhs = []
        self._compute_knm_matrices = []
        self._set_gouy_phases = []

        # Get any callbacks for the elements in the model
        # tell the element that we have now built the model and it
        # should do some initialisations for running simulations
        for el in self.model.elements.values():
            if hasattr(el, "_fill_rhs"):
                self._fill_rhs.append(el._fill_rhs)

            if hasattr(el, "_compute_knm_matrices"):
                self._compute_knm_matrices.append(el._compute_knm_matrices)

            if hasattr(el, "_set_gouy_phase"):
                self._set_gouy_phases.append(el._set_gouy_phase)

            if hasattr(el, "_on_build"):
                el._on_build(self)

    def solve(self):
        """
        Solves the current state of the simulation. `self.out` should be populated with the solution vector
        once this has been called.
        """
        raise NotImplementedError()

    def __enter__(self):
        """
        When entered the Simulation object will create the matrix to be used in
        the simulation.

        This is where the matrix gets allocated and constructed. It will expect
        that the model structure does not change after this until it has been
        exited.
        """
        raise NotImplementedError()

    def _unbuild(self):
        # let elements know that we have now finished this
        # simulation and do any cleanup if they need to
        for el in self.model.elements.values():
            if hasattr(el, "_on_unbuild"):
                el._on_unbuild(self)

    def __exit__(self, type_, value, traceback):
        raise NotImplementedError

    def _fill(self, carriers=None, zero_rhs=True, fill_rhs=True, fill_matrix=True):
        if self.is_modal:
            self.model.beam_trace(**self.model.beam_trace_args)
            self.compute_knm()
            self.set_spaces_gouy_phase()

        self._frequency_map = {f.f : f for f in self.frequencies}

        # fill_all True until we get a better method for detecting what stuff is being updated
        if fill_matrix:
            self.fill(fill_all=True, carrier=carriers)
        if zero_rhs:
            self._clear_rhs()
        if fill_rhs:
            self.fill_rhs(carriers)

    def run(self, carriers=None, zero_rhs=True, fill_rhs=True, fill_matrix=True):
        """Executes the simulation for model in its current state.

        Takes the following steps to compute an output:
         * Clears the RHS vector (if zero_rhs = True)
         * Fills the matrix (if keep_matrix = True)
         * Fills the RHS vector (if fill_rhs = True)
         * Solves
         * Fills the detector output list

        Results are saved in `Simulation.out` property.

        Parameters
        ----------
        carriers : :class:`Simulation`
            If running an audio sideband simulation then the carrier fields
            simulation object must be supplied.
        zero_rhs : boolean
            If true the input vector is not zeroed before filling. This is to enable you
            to inject custom intput vectors if desired.
        fill_rhs: boolean
            If True the input vector is filled, if not the current memory state will be used
        fill_matrix: boolean
            If True the matrix will be filled, if not the current memory state will be used
        Returns
        -------
        out : :class:`numpy.ndarray`
            The solved RHS vector.
        """
        if not self.model.is_built:
            raise Exception("Model has not been built")

        self._fill(carriers, zero_rhs, fill_rhs, fill_matrix)

        self.solve()

        # TODO ddb: if audio and quantum calcs needed
        if self.is_audio and False:
            self.fill_qnoise_rhs()

        return self.out

    def __copy__(self):
        raise Exception("Simulation objects can not be copied")

    def __deepcopy__(self, memo):
        raise Exception("Simulation objects can not be deepcopied")


