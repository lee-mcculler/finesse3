# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals
import numpy as np


def abs_sq(arr):
    return arr.real**2 + arr.imag**2


def reduceGE(
    SRABGE,
    node,
    matmul,
    matinv,
    matadd,
    N_limit_rel = 100,
    node_mark = lambda n : None,
):
    seq, req, req_alpha, seq_beta, seq_gamma, edge_map, = SRABGE
    #print("NODE:", node)

    self_edge = edge_map[node, node]

    CLG = matinv(-self_edge)

    #remove the self edge for the simplification stage
    seq[node].remove(node)
    req[node].remove(node)
    del edge_map[node, node]

    for snode in seq[node]:
        sedge = edge_map[node, snode]
        prod_L = matmul(sedge, CLG)

        for rnode in req[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)

            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod

            seq.setdefault(rnode, set()).add(snode)
            req.setdefault(snode, set()).add(rnode)

        for rnode in req_alpha[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)

            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod

            req_alpha.setdefault(snode, set()).add(rnode)

    for snode in seq_beta[node]:
        sedge = edge_map[node, snode]
        prod_L = matmul(sedge, CLG)

        for rnode in req[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)

            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod

            seq_beta.setdefault(rnode, set()).add(snode)

        for rnode in req_alpha[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)

            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod

            seq_gamma.setdefault(rnode, set()).add(snode)

    for snode in seq[node]:
        node_mark(snode)
        del edge_map[node, snode]
        req[snode].remove(node)
    del seq[node]

    for snode in seq_beta[node]:
        del edge_map[node, snode]
    del seq_beta[node]

    for rnode in req[node]:
        node_mark(rnode)
        del edge_map[rnode, node]
        seq[rnode].remove(node)
    del req[node]

    for rnode in req_alpha[node]:
        del edge_map[rnode, node]
    del req_alpha[node]
    return True


def reduceGE_testing(
    SRABGE,
    node,
    matmul,
    matinv,
    matadd,
    N_limit_rel,
    node_mark = lambda n : None,
):
    seq, req, req_alpha, seq_beta, seq_gamma, edge_map, = SRABGE
    #print("NODE:", node)

    self_edge = edge_map[node, node]

    CLG = matinv(-self_edge)

    #remove the self edge for the simplification stage
    seq[node].remove(node)
    req[node].remove(node)
    del edge_map[node, node]

    edge_map_update = dict()
    prod_L_update = dict()
    for snode in seq[node]:
        sedge = edge_map[node, snode]
        prod_L = matmul(sedge, CLG)
        prod_L_update[snode] = prod_L

        for rnode in req[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)
            if np.any(abs_sq(prod) > N_limit_rel):
                #GE failed from testing, need to reset and return False
                seq[node].add(node)
                req[node].add(node)
                edge_map[node, node] = self_edge
                return False
            prev_edge = edge_map_update.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map_update[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map_update[(rnode, snode)] = prod

    for snode in seq[node]:
        for rnode in req[node]:
            prod = edge_map_update[rnode, snode]
            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod
            seq.setdefault(rnode, set()).add(snode)
            req.setdefault(snode, set()).add(rnode)

        prod_L = prod_L_update[snode]
        for rnode in req_alpha[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)

            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod

            req_alpha.setdefault(snode, set()).add(rnode)
    del prod_L_update
    del edge_map_update

    for snode in seq_beta[node]:
        sedge = edge_map[node, snode]
        prod_L = matmul(sedge, CLG)

        for rnode in req[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)

            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod

            seq_beta.setdefault(rnode, set()).add(snode)

        for rnode in req_alpha[node]:
            redge = edge_map[rnode, node]
            prod = matmul(prod_L, redge)

            prev_edge = edge_map.get((rnode, snode), None)
            if prev_edge is not None:
                edge_map[(rnode, snode)] = matadd(prev_edge, prod)
            else:
                edge_map[(rnode, snode)] = prod

            seq_gamma.setdefault(rnode, set()).add(snode)

    for snode in seq[node]:
        node_mark(snode)
        del edge_map[node, snode]
        req[snode].remove(node)
    del seq[node]

    for snode in seq_beta[node]:
        del edge_map[node, snode]
    del seq_beta[node]

    for rnode in req[node]:
        node_mark(rnode)
        del edge_map[rnode, node]
        seq[rnode].remove(node)
    del req[node]

    for rnode in req_alpha[node]:
        del edge_map[rnode, node]
    del req_alpha[node]
    return True
