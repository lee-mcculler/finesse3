# -*- coding: utf-8 -*-
"""
Holds the various instances of simulation classes
"""

from .base import BaseSimulation
from .KLU import KLUSimulation
from .digraph import DigraphSimulation, DigraphSimulationBase
from .debug import DebugSimulation
from .dense import DenseSimulation
