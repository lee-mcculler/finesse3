"""
Transmissive optical components which focus or disperse light beams.
"""

from dataclasses import dataclass
import enum
import logging

import numpy as np

from finesse.knm import run_bayer_helms, zero_tem00_phase
from finesse.knm import log_knm_matrix
from finesse.utilities.maths import apply_ABCD as apply

from finesse.components.general import Connector, InteractionType
from finesse.components.node import NodeDirection, NodeType
from finesse.element import model_parameter, Rebuild
from finesse.utilities import refractive_indices

LOGGER = logging.getLogger(__name__)


class KnmLensNodeDirection(enum.IntEnum):
    LS12 = 1
    LS21 = 2

@dataclass
class LensKnmMatrices:
    K12: np.ndarray
    K21: np.ndarray

    K12_bh: np.ndarray
    K21_bh: np.ndarray

@model_parameter('f', np.inf, Rebuild.HOM, "_check_f")
class Lens(Connector):
    """
    A class representing a thin lens optical component with
    an associated focal length.
    """
    def __init__(self, name, f=np.inf):
        """Constructs a new `Lens` instance with the specified
        properties.

        Parameters
        ----------
        name : str
            Name of newly created `Lens` instance.

        f : float, optional
            Focal length of the lens in metres, defaults
            to infinity.
        """
        super().__init__(name)

        self.f = f

        self._add_port("p1", NodeType.OPTICAL)
        self.p1._add_node('i', NodeDirection.INPUT)
        self.p1._add_node('o', NodeDirection.OUTPUT)

        self._add_port("p2", NodeType.OPTICAL)
        self.p2._add_node('i', NodeDirection.INPUT)
        self.p2._add_node('o', NodeDirection.OUTPUT)

        # optic to optic couplings
        self._register_node_coupling(self.p1.i, self.p2.o,
                                     interaction_type=InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p2.i, self.p1.o,
                                     interaction_type=InteractionType.TRANSMISSION)

        self._knm_logging_info = {
            "matrices" : [], #set(["K12", "K21"]),
            "couplings" : None
        }

    def _check_f(self, value):
        if np.isclose(value, 0.0):
            raise ValueError("Focal length of lens must be non-zero.")
        self._recompute_abcd = True
        if self.has_model:
            self._model._retrace_required = True

        return value

    def ABCD(self, from_node, to_node, direction='x', symbolic=False):
        r"""Computes and returns the ABCD matrix of the lens. This is
        given by,

        .. math::
            M = \begin{pmatrix}
                    1 & 0 \\
                    -\frac{1}{f} & 1
                \end{pmatrix},

        where :math:`f` is the focal length of the lens.

        Parameters
        ----------
        from_node : :class:`.OpticalNode`
            Node to trace from.

        to_node : :class:`.OpticalNode`
            Node to trace to.

        direction: str, optional
            Direction of ABCD matrix computation, default is 'x'.

        Raises
        ------
        err : :class:`.NodeException`
            If either `from_node` or `to_node` are not in ``self.nodes``
            or if `from_node` and `to_node` are both inputs or both outputs.
        """
        self._ABCD_prechecks(from_node, to_node)

        if symbolic:
            raise NotImplementedError()
        else:
            f = self.f.value

        underlying = [
            [1.0, 0.0],
            [-1.0 / f, 1.0]
        ]

        M = np.array(underlying)

        return M

    def _on_build(self, sim):
        self._allocate_knm_matrices(sim)
        self._compute_knm_matrices(sim)
        self.K12 = self.knm_matrices.K12
        self.K21 = self.knm_matrices.K21

    def _fill_matrix(self, sim, carrier=None):
        for freq in sim.frequencies:
            sim[(self, "p1.i->p2.o", freq, freq)][:] = self.K12
            sim[(self, "p2.i->p1.o", freq, freq)][:] = self.K21

    def _allocate_knm_matrices(self, sim):
        I = np.eye(sim.nhoms, dtype=np.complex128)

        self.knm_matrices = LensKnmMatrices(
            K12=I.copy(), K21=I.copy(),
            K12_bh=I.copy(), K21_bh=I.copy()
        )

    def _compute_knm_matrix_bayer_helms(
        self, sim,
        qx1, qy1, qx2, qy2,
        nr, Knm
    ):
        run_bayer_helms(
            Knm,
            qx1, qy1, qx2, qy2,
            0.0, 0.0,
            nr,
            sim.homs,
            sim.model.lambda0,
            reverse_gouy=True
        )

    def _apply_knm_sequence(self, sim, qx1, qy1, qx2, qy2, nr1, nr2, direction):
        if direction == KnmLensNodeDirection.LS12:
            Knm = self.knm_matrices.K12
            Knm_bh = self.knm_matrices.K12_bh
            nr = nr2
        elif direction == KnmLensNodeDirection.LS21:
            Knm = self.knm_matrices.K21
            Knm_bh = self.knm_matrices.K21_bh
            nr = nr1

        self._compute_knm_matrix_bayer_helms(
            sim, qx1, qy1, qx2, qy2, nr, Knm_bh
        )

        zero_tem00_phase(Knm_bh, out=Knm)

    def _compute_knm_matrices(self, sim):
        if not sim.is_modal or not self._model._trace_changed:
            LOGGER.info("No knm dependent parameters have changed, skipping recomputation "
                        f"of knm matrices for: {self.name}")
            return

        LOGGER.info(f"Computing knm matrices for: {self.name}")

        # get beam parameters...
        node_q_map = sim.model.last_trace
        qx_p1o, qy_p1o, _ = node_q_map[self.p1.o]
        qx_p1i, qy_p1i, _ = node_q_map[self.p1.i]
        qx_p2o, qy_p2o, _ = node_q_map[self.p2.o]
        qx_p2i, qy_p2i, _ = node_q_map[self.p2.i]

        # ... and refractive indices of spaces attached to lens
        nr1, nr2 = refractive_indices(self.p1, self.p2)

        if nr1 == 0:
            raise ValueError(f"refractive index nr1 zero in _compute_knm_matrices of {self}")
        if nr2 == 0:
            raise ValueError(f"refractive index nr2 zero in _compute_knm_matrices of {self}")

        # compute propagations of q -> q' for each coupling
        # to be used as the input bases for computing knm
        qx_p1i_ptrns = apply(self.ABCD(self.p1.i, self.p2.o, 'x'), qx_p1i.q, nr1, nr2)
        qy_p1i_ptrns = apply(self.ABCD(self.p1.i, self.p2.o, 'y'), qy_p1i.q, nr1, nr2)
        qx_p2i_ptrns = apply(self.ABCD(self.p2.i, self.p1.o, 'x'), qx_p2i.q, nr2, nr1)
        qy_p2i_ptrns = apply(self.ABCD(self.p2.i, self.p1.o, 'y'), qy_p2i.q, nr2, nr1)

        # K12 calculations
        self._apply_knm_sequence(
            sim, qx_p1i_ptrns, qy_p1i_ptrns, qx_p2o.q, qy_p2o.q,
            nr1, nr2, KnmLensNodeDirection.LS12
        )

        # K21
        self._apply_knm_sequence(
            sim, qx_p2i_ptrns, qy_p2i_ptrns, qx_p1o.q, qy_p1o.q,
            nr1, nr2, KnmLensNodeDirection.LS21
        )

        matrices_to_log = self._knm_logging_info["matrices"]
        couplings_to_log = self._knm_logging_info["couplings"]
        if "K12" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K12, sim.homs, "K12 = \n", couplings_to_log))
        if "K21" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K21, sim.homs, "K21 = \n", couplings_to_log))
