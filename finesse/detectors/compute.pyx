from finesse.cmath.cmath cimport carg, conj, creal, cimag, csqrt # complex.h functions
from finesse.cmath.cmath cimport exp, sqrt # math.h functions
from finesse.cmath cimport complex_t # type: np.complex128_t (i.e. double complex)
from finesse.cmath cimport Math # namespace of common mathematical routines
from finesse.cmath cimport ComplexMath # namespace of complex mathematical routines
from finesse.cmath cimport Gaussian # namespace of Gaussian beam routines

cdef extern from "../constants.h":
    double complex COMPLEX_0 # 0 + 0i
    double complex COMPLEX_1 # 1 + 0i
    double complex COMPLEX_I # 0 + 1i

def ccd_output(
    DC, node, int[:, ::1] homs, complex_t qx, complex_t qy, double nr,
    double lambda0, double[::1] x, double[::1] y, complex_t[:, ::1] out,
    f=None
):
    cdef:
        int n, m
        Py_ssize_t freq_idx, field_idx, i, j
        complex_t at1, at2
        complex_t z_ij = COMPLEX_0
        double phase
        double xval, yval
        Py_ssize_t Nfields = homs.shape[0]
        Py_ssize_t Npts = x.shape[0]

    if f is not None:
        for i in range(Npts):
            xval = x[i]
            for j in range(Npts):
                yval = y[i]
                for freq in DC.frequencies:
                    if Math.float_eq(freq.f, f):
                        at1 = COMPLEX_0
                        for field_idx in range(Nfields):
                            n = homs[field_idx][0]
                            m = homs[field_idx][1]

                            phase = n * Gaussian.gouy(qx) + m * Gaussian.gouy(qy)

                            at1 += (
                                ComplexMath.u_nm(n, m, qx, qy, xval, yval, nr, lambda0)
                                * ComplexMath.z_by_phr(
                                    DC.get_DC_out(node, freq.index, field_idx), -phase
                                )
                            )

                        z_ij += at1

                out[i][j] = z_ij
