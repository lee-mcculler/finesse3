import numpy as np
import logging

LOGGER = logging.getLogger(__name__)

from finesse.detectors.general import Detector
from finesse.detectors.compute import ccd_output

class CCD(Detector):
    def __init__(self, name, node, xlim, ylim, npts, f=None):
        Detector.__init__(self, name, node, dtype=np.ndarray)
        self.f = f
        self.x = np.linspace(xlim[0], xlim[1], npts)
        self.y = np.linspace(ylim[0], ylim[1], npts)
        self._out = np.zeros((npts, npts), dtype=np.complex128)

    def get_output(self, DC, AC):
        node_q_map = DC.model.last_trace
        qx, qy, _ = node_q_map[self._node]

        # TODO (sjr) change so relative to beam waist size is optional
        x = qx.w0 * self.x
        y = qy.w0 * self.y
        nr = self._node.space.nr

        ccd_output(
            DC, self._node, DC.homs, qx.q, qy.q, nr, DC.model.lambda0, x, y,
            out=self._out, f=self.f
        )

        return self._out
