"""
Computes the laser power in an interferometer output or the power from an electrical signal.
"""

import numpy as np
import logging

from finesse.detectors.general import Detector
from finesse.element import model_parameter, Rebuild

LOGGER = logging.getLogger(__name__)

# TODO: DOCUMENT!!! This is a *very* confusing set of calculations.
@model_parameter("f0", None, Rebuild.Frequencies)
@model_parameter("f1", None, Rebuild.Frequencies)
@model_parameter("f2", None, Rebuild.Frequencies)
@model_parameter("f3", None, Rebuild.Frequencies)
@model_parameter("f4", None, Rebuild.Frequencies)
@model_parameter("f5", None, Rebuild.Frequencies)
@model_parameter("f6", None, Rebuild.Frequencies)
@model_parameter("f7", None, Rebuild.Frequencies)
@model_parameter("f8", None, Rebuild.Frequencies)
@model_parameter("f9", None, Rebuild.Frequencies)
@model_parameter("phase0", None, Rebuild.Frequencies)
@model_parameter("phase1", None, Rebuild.Frequencies)
@model_parameter("phase2", None, Rebuild.Frequencies)
@model_parameter("phase3", None, Rebuild.Frequencies)
@model_parameter("phase4", None, Rebuild.Frequencies)
@model_parameter("phase5", None, Rebuild.Frequencies)
@model_parameter("phase6", None, Rebuild.Frequencies)
@model_parameter("phase7", None, Rebuild.Frequencies)
@model_parameter("phase8", None, Rebuild.Frequencies)
@model_parameter("phase9", None, Rebuild.Frequencies)
class PowerDetector(Detector):
    """
    A class representing a power detector which calculates the laser power
    and phase at an output.
    """
    def __init__(self, name, node, freqs=[], phases=[]):
        """
        Constructs a new `PowerDetector` instance with the specified
        properties.

        Parameters
        ----------
        name : str
            Name of newly created :class:`.PowerDetector` instance.

        node : :class:`.Node`
            Node to read output from.

        freqs : [float], optional
            List of mixer demodulation frequencies (in Hz).

        phases : list[float], optional
            List of mixer demodulation phases (in Hz).
        """
        if len(freqs) == 0:
            self.__mode = "dc"
            dtype = np.float64
        elif len(phases) == len(freqs):
            self.__mode = "mixer"
            dtype = np.complex128
        elif len(phases) == len(freqs) - 1:
            self.__mode = "network"
            dtype = np.complex128
        else:
            raise ValueError("'phases' must be as long as or one less than 'freqs'")

        Detector.__init__(self, name, node, dtype=dtype, unit='W')

        if len(freqs) > 10:
            LOGGER.warn("PowerDetector only supports up to 10 demodulations.")
            freqs = freqs[:10]
            phases = phases[:10]
        for i, f in enumerate(freqs):
            setattr(self, f"f{i}", f)
        for i, p in enumerate(phases):
            setattr(self, f"phase{i}", p)

        self.__power_coeffs = {}

    def mask(self, n, m, factor):
        if not self._model.is_modal:
            raise ValueError(f"model is not modal, cannot set TEMs for laser: {self}")
        if n + m > self._model.maxtem:
            raise ValueError("n + m larger than maximum order for TEM.")
        self.__power_coeffs[(n, m)] = factor

    def get_output(self, DC, AC):
        # TODO: User-defined photodetectors
        power = 0
        values, is_changing = self._eval_parameters()
        freqs = []
        phases = []
        for k, v in values.items():
            if v is None:
                continue
            if k.startswith("f"):
                freqs.append(v)
            elif k.startswith("phase"):
                phases.append(v)

        freqs = np.array(freqs)
        phases = np.array(phases)

        if self.__mode == "dc":
            for hom, hom_idx in DC.model.mode_index_map.items():
                mode_power = 0
                for f1 in DC.frequencies:
                    for f2 in DC.frequencies:
                        if f1.f != f2.f:
                            continue

                        #outer = DC.out[DC.field(self._node, f1.index, hom_idx)]
                        #inner = DC.out[DC.field(self._node, f2.index, hom_idx)]
                        outer = DC.get_DC_out(self._node, f1.index, hom_idx)
                        inner = DC.get_DC_out(self._node, f2.index, hom_idx)
                        mode_power += outer * np.conj(inner)

                if hom in self.__power_coeffs:
                    power += mode_power * self.__power_coeffs[hom]
                else:
                    power += mode_power
            power = power.real * self._model._EPSILON0_C / 2    # Watts
        else:
            num_demod = len(freqs)
            num_mixes, fmix = self._create_mix_table(num_demod)
            z = np.zeros(num_mixes, dtype=np.complex128)
            for mix_index in range(num_mixes):
                # TODO: Is self.freqs correct here?
                f_ref = sum(fmix[mix_index] * freqs)
                z[mix_index] = self._get_mixed_amplitude(mix_index, f_ref, DC)
            z_tmp = self._set_demodulation_phase(num_demod, z, fmix, num_mixes, phases)
            power = self._get_demodulation_signal(num_demod, z_tmp, phases)

        return self.dtype(power)

    def _create_mix_table(self, num_demod):
        # TODO: Is the zeroth row/column important?
        num_mixes = 1
        total_num_mixes = int(num_demod * (num_demod - 1) / 2 + 1)
        fmix = np.ones((total_num_mixes, num_demod))
        fmix_tmp = np.ones(num_demod)
        for demod_index_outer in range(1, num_demod):
            for mix_permutation_index in range(demod_index_outer, 0, -1):
                num_mixes += 1
                fmix_tmp[mix_permutation_index - 1] *= -1
                fmix[num_mixes - 1][:] = fmix_tmp[:]
        return total_num_mixes, fmix

    def _get_mixed_amplitude(self, mix_index, f_ref, sim):
        amplitude = 0
        for hom, hom_idx in sim.model.mode_index_map.items():
            z = self._get_amplitude_sum(mix_index, f_ref, 1, sim, hom, hom_idx)
            if hom in self.__power_coeffs:
                amplitude += z * self.__power_coeffs[hom]
            else:
                amplitude += z
        return amplitude

    def _get_amplitude_sum(self, mix_index, f_ref, pd_factor, sim, hom, hom_idx):
        amplitude_sum = 0
        for f1 in sim.frequencies:
            for f2 in sim.frequencies:
                if np.isclose(f1.f - f2.f, f_ref) == 0:
                    # TODO: One signal, one carrier
                    factor = 1
                    #outer = sim.out[sim.field(self._node, f1.index, hom_idx)]
                    #inner = sim.out[sim.field(self._node, f2.index, hom_idx)]
                    outer = sim.get_DC_out(self._node, f1.index, hom_idx)
                    inner = sim.get_DC_out(self._node, f2.index, hom_idx)
                    amplitude_sum += (outer * np.conj(inner)
                                      * factor * pd_factor)
                    # TODO: is this correct? It seems wrong to re-apply this
                    # masking factor multiple times, but this is what finesse 2
                    # does
                    if hom in self.__power_coeffs:
                        amplitude_sum *= self.__power_coeffs[hom]
        return amplitude_sum

    def _set_demodulation_phase(self, num_demod, z, fmix, num_mixes, phases):
        phi = np.zeros(num_mixes)
        for demod_index in range(num_demod - 1):
            z1 = 0
            z2 = 0
            # Max phase
            if (phases[demod_index] == "max"):
                for mix_index in range(num_mixes):
                    if fmix[mix_index][demod_index] < 0:
                        z1 += z[mix_index]
                    else:
                        z2 += z[mix_index]
                phi_max = 0.5 * (np.angle(z2) - np.angle(z1))
                phases[demod_index] = phi_max
                for mix_index in range(num_mixes):
                    phi[mix_index] -= phi_max * fmix[mix_index][demod_index]
            # User-defined phase
            else:
                for mix_index in range(num_mixes):
                    phi[mix_index] -= (phases[demod_index]
                                       * fmix[mix_index][demod_index])
        z_tmp = 0
        for mix_index in range(num_mixes):
            z_tmp += z[mix_index] * np.exp(1j * phi[mix_index])
        return z_tmp

    def _get_demodulation_signal(self, num_demod, demod_amp, phases):
        z = demod_amp
        # Max phase
        if self.__mode == "network":
            pass
        elif (phases[-1] == "max"):
            z = np.abs(demod_amp)
        # User-defined phase
        else:
            z = (np.real(demod_amp) * np.cos(phases[num_demod - 1])
                 + np.imag(demod_amp) * np.sin(phases[num_demod - 1]))
        z *= 0.5**(num_demod - 1)
        # TODO: Transfer
        return z * self._model._EPSILON0_C / 2    # Watts
