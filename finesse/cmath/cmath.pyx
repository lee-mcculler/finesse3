"""Fast C functions for common mathematical routines
used across all the various Cython extensions of Finesse.

**This pure Cython extension is for those working on their own
Cython extensions for Finesse only. There are no Python functions
exposed by this sub-module, users of Finesse can safely ignore
this extension.**

.. note::
    **For developers:**

    The functions in this extension are to be used by the Cython extensions
    of Finesse, as they are optimised C-based functions only - no Python API
    is exposed.

    If a function you are writing requires exposure to Python such that it can be
    called from the non-Cython parts of Finesse, then use :mod:`finesse.utilities.maths`.

    See :ref:`cython_guide` for further details.
"""

cimport numpy as np
import numpy as np

cdef extern from "complex.h":
    double cabs(double complex z) nogil
    double complex cpow(double complex x, double complex y) nogil
    double cimag(double complex z) nogil
    double creal(double complex z) nogil
    double complex csqrt(double complex z) nogil
    double complex conj(double complex z) nogil

cdef extern from "math.h":
    double atan2(double y, double x) nogil
    double cos(double arg) nogil
    double exp(double arg) nogil
    double fabs(double arg) nogil
    double fmax(double x, double y) nogil
    double sin(double arg) nogil
    double sqrt(double arg) nogil

cdef extern from "../constants.h":
    long double PI
    double complex COMPLEX_0 # 0 + 0i
    double complex COMPLEX_1 # 1 + 0i

ctypedef np.complex128_t complex_t

#ctypedef fused real_numeric_t: # non-complex numeric type
#    short
#    int
#    long
#    float
#    double

cdef class ComplexMath:
    """A namespace of functions for complex mathematical routines."""
    @staticmethod
    cdef double abs_sqd(double complex z) nogil:
        return creal(z) * creal(z) + cimag(z) * cimag(z)

    @staticmethod
    cdef bint ceq(complex_t z1, complex_t z2) nogil:
        cdef:
            double maximum
            int re = 0
            int im = 0

        maximum = fmax(
            fmax(fabs(creal(z1)), fabs(creal(z2))),
            fmax(fabs(cimag(z1)), fabs(cimag(z2)))
        )

        if maximum < 1e-13: return True

        if creal(z1) == 0.0 and creal(z2) == 0.0:
            re = 1
        else:
            re = fabs(creal(z1) - creal(z2)) / maximum < 1e-13

        if cimag(z1) == 0.0 and cimag(z2) == 0.0:
            im = 1
        else:
            im = fabs(cimag(z1) - cimag(z2)) / maximum < 1e-13

        return re & im

    @staticmethod
    cdef complex_t pow_complex(complex_t z, double n) nogil:
        # NOTE cpow is implementation-dependent, so adding check here
        #      first to avoid problems with (0+0i)^0
        if n == 0.0:
            return COMPLEX_1
        return cpow(z, n)

    @staticmethod
    cdef int inv_complex(complex_t* res, complex_t z) except -1:
        cdef double inv_abs_sqd_z

        if ComplexMath.ceq(z, COMPLEX_0):
            raise ValueError("Complex division by zero.")

        inv_abs_sqd_z = 1.0 / ComplexMath.abs_sqd(z)

        res[0] = creal(z) * inv_abs_sqd_z - 1.0j * cimag(z) * inv_abs_sqd_z
        return 0

    @staticmethod
    cdef complex_t inv_complex_unsafe(complex_t z) nogil:
        cdef double inv_abs_sqd_z = 1.0 / ComplexMath.abs_sqd(z)

        return creal(z) * inv_abs_sqd_z - 1.0j * cimag(z) * inv_abs_sqd_z

    @staticmethod
    cdef complex_t z_by_phr(complex_t z, double ph) nogil:
        cdef:
            complex_t zz
            double cph = cos(ph)
            double sph = sin(ph)

        zz = creal(z) * cph - cimag(z) * sph + 1j * (creal(z) * sph + cimag(z) * cph)

        return zz

    @staticmethod
    cdef complex_t u_nm(
        int n, int m, complex_t qx, complex_t qy,
        double x, double y, double nr, double lambda0
    ):
        cdef:
            complex_t zx, zy

        zx = ComplexMath.u_single_plane(n, qx, x, nr, lambda0)
        zy = ComplexMath.u_single_plane(m, qy, y, nr, lambda0)

        return zx * zy

    @staticmethod
    cdef complex_t u_single_plane(int n, complex_t q, double x, double nr, double lambda0):
        cdef:
            double factor
            complex_t phase, z1, z2, z3
            double k = 2.0 * PI / lambda0 * nr
            np.ndarray[double, ndim=1] herm_coeffs = np.zeros(1+n)

        herm_coeffs[-1] = 1.0

        # TODO (sjr) use custom hermite function with hard-coded numbers instead
        factor = (
            (2.0/PI)**0.25 / sqrt(2**n * Math.factorial(n) * Gaussian.w0_size(q, nr, lambda0))
            * np.polynomial.hermite.hermval(sqrt(2) * x / Gaussian.w_size(q, nr, lambda0), herm_coeffs)
        )

        z1 = csqrt((0.0 + cimag(q)*1j) / q)
        z2 = csqrt((0.0 + cimag(q)*1j) * conj(q) / ((0.0 - cimag(q)*1j) * q))
        z3 = z1 * ComplexMath.pow_complex(z2, n)

        phase = (-1.0 * k * x * x / 2.0) * ComplexMath.inv_complex_unsafe(q)

        factor *= exp(-1.0 * cimag(phase))

        return factor * ComplexMath.z_by_phr(z3, creal(phase))

cdef class Gaussian:
    """A namespace of functions for Gaussian beam physics."""
    @staticmethod
    cdef double gouy(complex_t q) nogil:
        return atan2(creal(q), cimag(q))

    @staticmethod
    cdef double w0_size(complex_t q, double nr, double lambda0) nogil:
        return sqrt(lambda0 * cimag(q) / (PI * nr))

    @staticmethod
    cdef double w_size(complex_t q, double nr, double lambda0) nogil:
        return cabs(q) * sqrt(lambda0 / (nr * PI * cimag(q)))

    @staticmethod
    cdef double z_q(complex_t q) nogil:
        return creal(q)

    @staticmethod
    cdef double z_R(complex_t q) nogil:
        return cimag(q)

cdef class Math:
    """A namespace of functions for non-complex mathematical routines."""
    @staticmethod
    cdef double factorial(int n) nogil:
        cdef:
            int i
            double ret

        ret = 1.0
        for i in range(1, n+1):
            ret *= i

        return ret

    @staticmethod
    cdef double msign(int n) nogil:
        return -1.0 if n % 2 else 1.0

    @staticmethod
    cdef int nmin(int n, int m) nogil:
        return n if n < m else m

    @staticmethod
    cdef bint float_eq(double x, double y) nogil:
        if x == 0.0 and y == 0.0:
            return 1
        else:
            return fabs(x - y) / fmax(fabs(x), fabs(y)) < 1e-13
