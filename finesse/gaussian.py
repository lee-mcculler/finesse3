"""
Gaussian beam related tools and utilities.
"""

import math
import numpy as np

from finesse.utilities.maths import complex_equal

class BeamParam(object):
    """
    Gaussian beam complex parameter.

    `BeamParam` is effectively a complex number with extra
    functionality to determine beam parameters. The wavelength
    of light and index of refraction of the medium default to
    :math:`\lambda = 1064\,\mathrm{nm}` and :math:`n_r = 1`,
    respectively.

    The following are legal initialisations of a `BeamParam`
    object::

        q = BeamParam(w0=w0, z=z)
        q = BeamParam(z=z, zr=zr)
        q = BeamParam(w=w, rc=rc)
        q = BeamParam(q=c) # where c is a complex number
        qnull = BeamParam() # a null beam parameter

    where :math:`w_0` is the radius of the beam at the waist-position
    :math:`z_0`, and :math:`z_R` is the Rayleigh range.

    The default wavelength and refractive index values can also be
    changed with (for example)::

        q = BeamParam(wavelength, nr, w0=w0, zr=zr)
    """
    def __init__(self, wavelength=1064e-9, nr=1, *args, **kwargs):
        """
        Constructs a new `BeamParam` instance with the specified properties.

        Parameters
        ----------
        wavelength : float, optional
            Wavelength of the beam light, defaults to 1064 nm.

        nr : float, optional
            Refractive index, defaults to unity.
        """
        self.__q = None
        self.__lambda = wavelength
        self.__nr = nr

        if len(args) == 1:
            self.__q = complex(args[0])

        elif len(kwargs) == 1:
            if "q" in kwargs:
                self.__q = complex(kwargs["q"])
            else:
                raise Exception("Must specify: z and w0 or z and zr or rc and w or q, to define the beam parameter")

        elif len(kwargs) == 2:

            if "w0" in kwargs and "z" in kwargs:
                q = kwargs["z"] + 1j * math.pi*kwargs["w0"]**2/(self.__lambda/self.__nr)
            elif "z" in kwargs and "zr" in kwargs:
                q = kwargs["z"] + 1j * kwargs["zr"]
            elif "rc" in kwargs and "w" in kwargs:
                one_q = 1 / kwargs["rc"] - 1j * wavelength / (math.pi * nr * kwargs["w"]**2)
                q = 1/one_q
            else:
                raise Exception("Must specify: z and w0 or z and zr or rc and w or q, to define the beam parameter")

            self.__q = q
        else:
            raise Exception("Incorrect usage for gauss_param constructor")

    @property
    def wavelength(self):
        """The wavelength of the beam (in metres).

        :getter: Returns the wavelength of the beam.
        :setter: Sets the wavelength of the beam.
        """
        return self.__lambda

    @wavelength.setter
    def wavelength(self,value): self.__lambda = value

    def __repr__(self):
        return "<%s (w0=%s, w=%s, z=%s) at %s>" % (self.__class__.__name__, self.w0, self.w, self.z, hex(id(self)))

    @property
    def nr(self):
        """The refractive index associated with
        the `BeamParam`.

        :getter: Returns the index of refraction.
        :setter: Sets the index of refraction.
        """
        return self.__nr

    @property
    def q(self):
        """The complex beam parameter value (:math:`q`).

        :getter: Returns the complex beam parameter value.
        """
        return self.__q

    @property
    def z(self):
        """The relative distance to the waist of
        the beam (in metres).

        :getter: Returns the relative distance to the waist.
        :setter: Sets the relative distance to the waist.
        """
        return self.__q.real

    @z.setter
    def z(self, value):
        self.__q = complex(1j*self.__q.imag + float(value))

    @property
    def zr(self):
        """The Rayleigh range (:math:`z_R`) of the
        beam (in metres).

        :getter: Returns the Rayleigh range.
        :setter: Sets the Rayleigh range.
        """
        return self.__q.imag

    @zr.setter
    def zr(self, value):
        self.__q = complex(self.__q.real + 1j*float(value))

    @property
    def w(self, z=None):
        return np.abs(self.__q)* np.sqrt(self.__lambda / (self.__nr * math.pi * self.__q.imag))

    def beamsize(self, z=None, wavelength=None, nr=None, w0=None):
        """Computes the radius of the beam at a specified distance from/to
        the beam waist position.

        Parameters
        ----------
        z : float, optional
            Distance along optical axis relative to waist, defaults
            to waist-position.

        wavelength : float, optional
            Wavelength of the beam to use, defaults to the stored value.

        nr : float, optional
            Refractive index to use, defaults to the stored value.

        w0 : float, optional
            Waist-size of the beam, defaults to the stored value.

        Returns
        -------
        float
            The radius of the beam using the specified properties.
        """
        if z is None:
            z = self.z
        else:
            z = np.array(z)

        if wavelength is None:
            wavelength = self.wavelength
        else:
            wavelength = np.array(wavelength)

        if nr is None:
            nr = self.nr
        else:
            nr = np.array(nr)

        if w0 is None:
            w0 = self.w0
        else:
            w0 = np.array(w0)

        q = z + 1j * math.pi * w0 **2 / (wavelength/nr)

        return np.abs(q)*np.sqrt(wavelength / (nr * math.pi * q.imag))

    def gouy(self, z=None, wavelength=None, nr=None, w0=None):
        """Computes the Gouy-phase at a specified distance from/to
        the beam waist position.

        Parameters
        ----------
        z : float, optional
            Distance along optical axis relative to waist, defaults
            to waist-position.

        wavelength : float, optional
            Wavelength of the beam to use, defaults to the stored value.

        nr : float, optional
            Refractive index to use, defaults to the stored value.

        w0 : float, optional
            Waist-size of the beam, defaults to the stored value.

        Returns
        -------
        float
            The accumulated Gouy phase using the specified properties.
        """
        if z is None:
            z = self.z
        else:
            z = np.array(z)

        if wavelength is None:
            wavelength = self.wavelength
        else:
            wavelength = np.array(wavelength)

        if nr is None:
            nr = self.nr
        else:
            nr = np.array(nr)

        if w0 is None:
            w0 = self.w0
        else:
            w0 = np.array(w0)

        q = z + 1j * math.pi * w0 **2 / (wavelength*nr)

        return np.arctan2(q.real, q.imag)

    @property
    def divergence(self):
        r"""Divergence of the beam.

        The divergence is defined as,

        .. math::

            d = \frac{\lambda}{w_0 \pi},

        where :math:`\lambda` is the wavelength and :math:`w_0`
        is the waist size of the beam.

        :getter: Returns the beam divergence.
        """
        return self.wavelength/ (self.w0 * np.pi)

    @property
    def w0(self):
        """The radius of the waist of the beam (in metres).

        :getter: Returns the beam waist-size.
        :setter: Sets the beam waist-size.
        """
        return np.sqrt(self.__q.imag * self.__lambda / (self.__nr * math.pi))
    @w0.setter
    def w0(self,value ):
        self.__q = complex(self.__q.real + 1j*value**2 * (self.__nr * math.pi)/self.__lambda)

    @property
    def Rc(self):
        """Radius of curvature of the beam (in metres).

        :getter: Returns the beams' radius of curvature.
        """
        def __rc(z, zr):
            if z != 0:
                return z * (1 + (zr/z)**2)
            else:
                return float("inf")

        v = np.vectorize(__rc)

        return v(self.z, self.zr)

    def curvature(self, z=None, wavelength=None, nr=None, w0=None):
        """Curvature of the beam as a function of the optical
        axis distance.

        Parameters
        ----------
        z : array like, optional
            Optical axis distance, defaults to ``self.z``.

        wavelength : float, optional
            Beam wavelength, defaults to ``self.wavelength``.

        nr : float, optional
            Index of refraction, defaults to ``self.nr``.

        w0 : float, optional
            Waist-size of the beam, defaults to ``self.w0``.

        Returns
        -------
        Curvature of the beam.
        """
        if z is None:
            z = self.z
        else:
            z = np.array(z)

        if wavelength is None:
            wavelength = self.wavelength
        else:
            wavelength = np.array(wavelength)

        if nr is None:
            nr = self.nr
        else:
            nr = np.array(nr)

        if w0 is None:
            w0 = self.w0
        else:
            w0 = np.array(w0)

        q = z + 1j * math.pi * w0 **2 / (wavelength*nr)

        return q.real * (1+ (q.imag/q.real)**2)

    @staticmethod
    def overlap(q1, q2):
        r"""
        Computes the projection from one beam parameter to another to give a measure of the
        overlap between the two beam parameters. The quantity computed is,

        .. math::
            \mathcal{O} = \frac{4|\Im{\{q_1\}}\,\Im{\{q_2\}}|}{|q_1^* - q_2|^2}.

        This function was provided by Paul Fulda and Antonio Perreca, which came originally
        from Chris Mueller.

        Parameters
        ----------
        q1 : :class:`.BeamParam`
            First beam parameter.

        q2 : :class:`.BeamParam`
            Second beam parameter.

        Returns
        -------
        overlap : float
            The overlap between `q1` and `q2` as defined above.
        """
        return abs(4*q1.imag * q2.imag)/abs(q1.conjugate()-q2)**2


    @staticmethod
    def mismatch(q1, q2):
        """
        The mismatch parameter (1-overlap) as taken from the Bayer-Helms paper.
        This expression for mismatch is less susceptible to float rounding than
        just 1-overlap for tiny mismatches ( M < 1e-16 )

        Added by Alexei Ciobanu on 02/05/2018
        """
        return abs(q1-q2)**2/abs(q1-q2.conjugate())**2

    @staticmethod
    def overlap_contour(q1, M, t):
        """
        This function returns a set of beam parameters that are mismatched to q1 by
        an overlap M. There are multiple beam parameters that can be X% overlapped
        with one particular q value. This function is parameterised with t from 0
        to 2pi, which can provide all the possible beam parameters that are M% mismatched.

        q1 - reference beam parameter
        M  - Mismatch factor (1-overlap) [0 -> 1]
        t  - Selection parameter [0 -> 2pi]

        Examples
        --------
        Plots the contours of mismatch for 0.1% and 1% from some initial q value::

            import numpy as np
            import matplotlib.pyplot as plt
            import finesse

            qin = finesse.BeamParam(w0=1e-3, z=20)
            t = np.linspace(0, 2*np.pi, 100)

            # use vectorised functions to select a cerain property of the beam paramters
            vx  = np.vectorize(lambda q: q.z)
            vy  = np.vectorize(lambda q: q.w/1e-3)

            for mm in [1e-3, 2e-2]:
                mmc = finesse.BeamParam.overlap_contour(qin, mm, t)

                plt.text(vx(mmc[20]), vy(mmc[20]), "%1.1f%%" % ((mm*100)),alpha=0.5, fontsize=8)
                l, = plt.plot(vx(mmc),     vy(mmc),     ls='--', alpha=0.2, zorder=-10, c='k')

            plt.show()

        """
        from numpy import vectorize
        assert(M < 1 and M >= 0)
        assert(q1.imag > 0)

        vbp = vectorize(lambda x: BeamParam(self.__lambda, self.__nr, x))

        z1 = np.real(q1)
        zR1 = np.imag(q1)
        r = (2*np.sqrt(M)*zR1)/(1-M)
        y0 = ((M+1)*zR1)/(1-M)
        x0 = z1
        q2 = r*np.cos(t) + x0 + 1j*(r*np.sin(t) + y0)

        return vbp(q2)


    def conjugate(self):
        """Computes and returns the complex conjugate of the beam parameter.

        Returns
        -------
        q_conj : :class:`.BeamParam`
            The complex conjugate of this `BeamParam` instance.
        """
        return BeamParam(self.__lambda, self.__nr, self.__q.conjugate())

    def __abs__(self):
        return abs(complex(self.__q))

    def __complex__(self):
        return self.__q

    def __str__(self):
        return str(self.__q)

    def __mul__(self, a):
        return BeamParam(self.__lambda, self.__nr, self.__q * complex(a))

    def __imul__(self, a):
        self.__q *= complex(a)
        return self

    __rmul__ = __mul__

    def __add__(self, a):
        return BeamParam(self.__lambda, self.__nr, self.__q + complex(a))

    def __iadd__(self, a):
        self.__q += complex(a)
        return self

    __radd__ = __add__

    def __sub__(self, a):
        return BeamParam(self.__lambda, self.__nr, self.__q - complex(a))

    def __isub__(self, a):
        self.__q -= complex(a)
        return self

    def __rsub__(self, a):
        return BeamParam(self.__lambda, self.__nr, complex(a) - self.__q)

    def __div__(self, a):
        return BeamParam(self.__lambda, self.__nr, self.__q / complex(a))

    def __truediv__(self, a):
        return BeamParam(self.__lambda, self.__nr, self.__q / complex(a))

    def __idiv__(self, a):
        self.__q /= complex(a)
        return self

    def __pow__(self, q):
        return BeamParam(self.__lambda, self.__nr, self.__q**q)

    def __neg__(self):
        return BeamParam(self.__lambda, self.__nr, -self.__q)

    def __eq__(self, q):
        if q is None:
            return False

        return complex_equal(complex(q), self.__q)

    @property
    def real(self):
        """The real part of the complex beam parameter, equal to
        the relative distance to the beam waist (in metres).

        :getter: Returns the real part of the beam parameter.
        :setter: Sets the real part of the beam parameter.
        """
        return self.__q.real
    @real.setter
    def real(self, value): self.__q.real = value

    @property
    def imag(self):
        """The imaginary part of the complex beam parameter, equal
        to the Rayleigh range :math:`z_R` of the beam (in metres).

        :getter: Returns the imaginary part of the beam parameter.
        :setter: Sets the imaginary part of the beam parameter.
        """
        return self.__q.imag
    @imag.setter
    def imag(self, value): self.__q.imag = value

    def reverse(self):
        """Reverses the direction of the beam parameter by setting
        `q.real = - q.real`."""
        self.__q = -1.0 * self.__q.real + 1j * self.__q.imag
