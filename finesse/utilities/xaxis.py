"""
A collection of functions that run simulations.
"""

import numpy as np
import logging

from finesse.exceptions import ParameterLocked

LOGGER = logging.getLogger(__name__)

def step(carrier, signal):
    carrier.run()

    if signal is not None:
        signal.run(carrier)


def run_axes(*args, model=None, **kwargs):
    """
    This function will run the model by scanning multiple parameters over individual axes.
    This is similar in functionality to the xaxis, x2axis, x3axis commands in Finesse v2
    except it is far more general. Any number of axes and parameters can be scanned over
    if requested.

    The axis values will be stored in the returned output object in the variables out.x1,
    out.x2, ..., out.xN.

    Parameters
    ----------
    *args
        pair of parameter and array to scan it over

    **kwargs
        Passed to `Model.Build`

    Examples
    --------

    >>> ifo = Model()
    >>> l1 = Laser('l1',  P=1)
    >>> pd = Photodiode('pd')
    >>> ifo.connect(l1, pd)
    >>> ifo.add( PowerDetector('P', pd.n1.i) )
    >>> out = ifo.run_axes(ifo.l1.P, np.linspace(0,1,3), l1.phase, np.linspace(90,180,5))
    >>> print(out.P, out.x1, out.x2)
    """
    from finesse.analysis import Output

    if len(args) == 0 and model is not None:
        models = [model]

    elif len(args) % 2 != 0:
        raise Exception("Arguments must be pairs of parameter followed by an array of values to scan over")

    params = args[::2]
    axes   = tuple(np.atleast_1d(_) for _ in args[1::2])

    LOGGER.info("Scanning parameters %s", list(params))

    if len(args) > 0:
        models = [p._model for p in np.atleast_1d(params)]

        if models.count(models[0]) != len(models):
            raise Exception("Parameters provided are associated with different models")

        if models[0].is_built:
            for p in params:
                if not p.is_tunable:
                    raise ParameterLocked(f"{repr(p)} must set as tunable before buulding the simulation")
        else:
            for p in params:
                p.is_tunable = True

    ifo = models[0]
    out_shape = tuple(np.size(_) for _ in axes)

    if ifo.is_built:
        allocated = True
        carrier = ifo.carrier_simulation
        try:
            signal  = ifo.signal_simulation
        except AttributeError:
            signal = None
    else:
        allocated = False
        sims = ifo.build(**kwargs)

        if len(sims) == 1:
            carrier, signal = sims[0], None
        elif len(sims) == 2:
            carrier, signal = sims
        else:
            raise Exception("Unexpected number of Simulation objects")

    out = Output(ifo, out_shape)

    for i, value in enumerate(axes):
        setattr(out, f"x{i+1}", value)

    try:
        if not allocated:
            carrier.__enter__()
            if signal is not None:
                signal.__enter__()

        # Now we loop over the actual simulation and run each point
        shapes = tuple(np.size(_) for _ in axes)

        for idx in np.ndindex(shapes):
            for p, ax, i in zip(params, axes, idx):
                p.value = ax[i]

            step(carrier, signal)

            out.update(idx)
    finally:
        # If the passed model was already built then we need
        # to clean up after ourselves
        if not allocated:
            carrier.__exit__(None, None, None)
            if signal is not None:
                signal.__exit__(None, None, None)

            ifo.unbuild()

            for p in params:
                p.is_tunable = False

    return out

def noxaxis(model, **kwargs):
    return run_axes(model=model, **kwargs)

def xaxis(param, start, stop, steps, mode='lin', **kwargs):
    """
    Runs a model to scan a parameter between two points for a number of steps.
    The model that is run is retrieved from the parameter reference.

    This should provide an equivalent to the xaxis command in Finesse v2.

    Parameters
    ----------
    param : :class:`.ModelParameter`
        Parameter of component to scan
    start, stop : float
        Start and end values of the scan
    steps : int
        Number of steps between start and end
    mode : str
        'lin' or 'log' for linear or logarithmic step sizes
    **kwargs
        Passed to `Model.Build`

    Returns
    -------
    output : :class:`.Output`
    """
    if mode == 'lin':
        return run_axes(param, np.linspace(start, stop, steps + 1), **kwargs)
    else:
        return run_axes(param, np.logspace(np.log10(start), np.log10(stop), steps + 1), **kwargs)

def x2axis(param1, start1, stop1, steps1, param2, start2, stop2, steps2, mode1='lin', mode2='lin', **kwargs):
    """
    Runs a model to scan a parameter between two points for a number of steps.
    The model that is run is retrieved from the parameter reference.

    This should provide an equivalent to the x2axis command in Finesse v2.

    param2 is in the inner loop and param1 in the outer loop.

    Parameters
    ----------
    param1, param2 : :class:`.ModelParameter`
        Parameter of component to scan
    start1, stop1, start2, stop2 : float
        Start and end values of the scan
    steps1, steps2 : int
        Number of steps between start and end
    mode1, mode2 : str
        'lin' or 'log' for linear or logarithmic step sizes for axis 1 and 2
    **kwargs
        Passed to `Model.Build`

    Returns
    -------
    output : :class:`.Output`
    """
    if mode1 == 'lin':
        steps1 = np.linspace(start1, stop1, steps1 + 1)
    else:
        steps1 = np.logspace(np.log10(start1), np.log10(stop1), steps1 + 1)

    if mode2 == 'lin':
        steps2 = np.linspace(start2, stop2, steps2 + 1)
    else:
        steps2 = np.logspace(np.log10(start2), np.log10(stop2), steps2 + 1)

    return run_axes(param1, steps1,
                    param2, steps2, **kwargs)





