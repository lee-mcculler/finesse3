"""
Tools for tracing Gaussian beams through a Finesse :class:`.Model`
instance.

Listed below are all the sub-modules of the ``tracing`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: tracing/
    {contents}
"""

import enum

class TraceType(enum.Enum):
    """Enum determining the type of tracing algorithm
    to be run by a :class:`.Tracer` object.
    """
    SIMULATION = 0
    UTILITY = 1
    RT_ABCD = 2
    ABCD = 3
    CAVPOWER = 4

from finesse.tracing.tracer import Tracer
from finesse.tracing.tracesolution import TraceSolution, TraceDataEntry

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    __doc__ = __doc__.format(contents=_collect_submodules("tracing"))
