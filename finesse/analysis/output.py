"""Simulation output"""

import numpy as np
import logging

LOGGER = logging.getLogger(__name__)


class Output:
    """
    This in an object that holds outputs from running a simulation. It is
    essentially a wrapped up Numpy structured array whose named elements are
    the names of outputs in a model.

    Detectors are stored in the array by their name. So you can use::

        output['detector_name']

    or, if the key has an attribute called `name` (as all Finesse.detectors do)
    it will use that, so using::

        output[ifo.detector]

    will return the same values.

    The underlying storage format is a Numpy structured array. You can select
    runs by::

        output[ifo.detector][a:b:c]

    where `a:b:c` is your slice. Or you can select multiple outputs with::

        output[['det1', 'det2']][a:b:c]
    """

    def __init__(self, model, shape):
        """
        Constructs a new `Output` object from the specified `model`.

        Parameters
        ----------
        model : :class:`.Model`
            Model object to get simulation data from

        shape : :class:`numpy.ndarray`
            The shape of the underlying data array. use a single integer for 1D
            outputs, N-dimensional outputs can be specified by using tuples, i.e.
            (10,5,100) for a 3D array with the requested sizes.
        """
        self._outputs = {}
        self._num = -1
        self._model = model
        self._units  = []
        self._axes = None

        dtypes = []

        for det in model.detectors:
            dtypes.append((det.name, det.dtype))

        LOGGER.info("Outputting shape=%s dtype=%s", shape, dtypes)

        self._dtype   = np.dtype(dtypes)
        self._outputs = np.zeros(shape, dtype=self._dtype)

    def __getattr__(self, key):
        return self[key]

    def __str__(self):
        return str(self._outputs)

    @property
    def axes(self): return self._axes

    @property
    def entries(self):
        """
        The number of outputs that have been stored in here so far
        """
        return self._num + 1

    @property
    def shape(self):
        return self._outputs.shape

    @property
    def outputs(self):
        """
        Returns all the outputs that have been stored.
        """
        return self._Output__outputs.dtype.names

    def expand(self, shape):
        """
        Expands the output buffer by `shape` elements. This will be slow if you
        call repeatedly to increase by just a few elements as the whole array
        is copied into a new block of memory.

        Parameters
        ==========
        shape
            Shape of new array to make
        """
        self._outputs = np.append(self._outputs, np.empty(shape, dtype=self._dtype))

    def __getitem__(self, key):
        if hasattr(key, 'name'):
            return self._outputs[key.name]
        else:
            return self._outputs[key]

    def update(self, index):
        """
        Calling this will compute all detector outputs and add an entry to
        the outputs stored. Calling it multiple times without re-running
        the simulation will result in duplicate entries.

        Parameters
        ==========

        index : (int, ...)
           Index to calculate the outputs for, use tuples of N-size for
           N-dimensional outputs
        """
        for det in self._model.detectors:
            o = det.get_output(self._model.carrier_simulation, self._model.signal_simulation)
            self[det][index] = o

        self._num += 1
