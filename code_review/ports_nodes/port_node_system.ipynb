{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import finesse\n",
    "import finesse.components as fc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finesse 3 Code Review: The port and node system\n",
    "\n",
    "### *Review date: 20/06/2019*\n",
    "\n",
    "One of the key structural improvements made to Finesse is the introduction of a flexible port and node system. This system underpins the code structure of Finesse as all couplings and interactions are performed via ports and nodes of components.\n",
    "\n",
    "A `Port` in Finesse 3 is fundamentally similar to the definition of a node in Finesse 2; i.e. it represents a point of connections at any arbitrary component. These connection points are then `Node` instances which represent the direction of the coupling into/from the component and also the type of the connection. This connection type is a new feature for Finesse 3 as previous versions only allowed optical nodes at a component and in a model, whereas Finesse 3 has three different node types (see `NodeType`) - optical, electrical and mechanical - with inter-couplings allowed between these different nodes.\n",
    "\n",
    "<img src=\"mirror_optical_ports.svg\">\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing ports\n",
    "\n",
    "Ports of a component can be accessed via the component through the names that they were assigned during construction. For example, a `Mirror` has two ports (`n1` and `n2`) which can be grabbed directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<Port M1.p1 @ 0x7f93213b96d8>\n",
      "<Port M1.p2 @ 0x7f93213b97f0>\n"
     ]
    }
   ],
   "source": [
    "M1 = fc.Mirror(\"M1\")\n",
    "print(M1.p1)\n",
    "print(M1.p2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can also get a *read-only tuple* of all the ports of a component:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(<Port M1.p1 @ 0x7f93213b96d8>, <Port M1.p2 @ 0x7f93213b97f0>, <Port M1.mech @ 0x7f93213b9978>, <Port M1.phase_sig @ 0x7f93213b9a90>)\n"
     ]
    }
   ],
   "source": [
    "print(M1.ports)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What does a port contain?\n",
    "\n",
    "In the example code below we show the main properties of a `Port` - namely its type, the nodes that it holds and the component that it is attached to (which is always a `Space` instance for optical connections)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Port M1.n2 name = p2\n",
      "Port M1.n2 type = NodeType.OPTICAL\n",
      "Port M1.n2 owning component = <'M1' @ 0x7f93213b9198 (Mirror)>\n",
      "Port M1.n2 attached component = <'M1_M2' @ 0x7f93507d0ac8 (Space)>\n",
      "Port M1.n2 nodes = (<OpticalNode M1.p2.i @ 0x7f93213b9828>, <OpticalNode M1.p2.o @ 0x7f93213b98d0>)\n"
     ]
    }
   ],
   "source": [
    "# make another mirror\n",
    "M2 = fc.Mirror(\"M2\")\n",
    "\n",
    "model = finesse.Model()\n",
    "# and connect M1 <-> M2 in a model via a Space of length 1m\n",
    "model.chain(M1, fc.Space(\"M1_M2\", L=10), M2)\n",
    "\n",
    "print(f\"Port M1.n2 name = {M1.p2.name}\")\n",
    "print(f\"Port M1.n2 type = {M1.p2.type}\")\n",
    "print(f\"Port M1.n2 owning component = {M1.p2.component}\")\n",
    "print(f\"Port M1.n2 attached component = {M1.p2.attached_to}\")\n",
    "print(f\"Port M1.n2 nodes = {M1.p2.nodes}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing nodes\n",
    "\n",
    "#### Via a component\n",
    "\n",
    "To access all the nodes of a component:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "All nodes of M1 = {'M1.p1.i': <OpticalNode M1.p1.i @ 0x7f93213b9668>, 'M1.p1.o': <OpticalNode M1.p1.o @ 0x7f93213b9748>, 'M1.p2.i': <OpticalNode M1.p2.i @ 0x7f93213b9828>, 'M1.p2.o': <OpticalNode M1.p2.o @ 0x7f93213b98d0>, 'M1.mech.z': <MechanicalNode M1.mech.z @ 0x7f93213b99b0>, 'M1.mech.yaw': <MechanicalNode M1.mech.yaw @ 0x7f93213b9a20>, 'M1.mech.pitch': <MechanicalNode M1.mech.pitch @ 0x7f93213b9a58>, 'M1.phase_sig.i': <ElectricalNode M1.phase_sig.i @ 0x7f93213b9ac8>, 'M1.phase_sig.o': <ElectricalNode M1.phase_sig.o @ 0x7f93213b9b00>}\n"
     ]
    }
   ],
   "source": [
    "print(f\"All nodes of M1 = {M1.nodes}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or all the optical nodes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Optical nodes of M1 = (<OpticalNode M1.p1.i @ 0x7f93213b9668>, <OpticalNode M1.p1.o @ 0x7f93213b9748>, <OpticalNode M1.p2.i @ 0x7f93213b9828>, <OpticalNode M1.p2.o @ 0x7f93213b98d0>)\n"
     ]
    }
   ],
   "source": [
    "print(f\"Optical nodes of M1 = {M1.optical_nodes}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get a single optical node of a component by its *direction*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Input node of port M1.n1 = <OpticalNode M1.p1.i @ 0x7f93213b9668>\n",
      "Output node of port M1.n1 = <OpticalNode M1.p1.o @ 0x7f93213b9748>\n"
     ]
    }
   ],
   "source": [
    "print(f\"Input node of port M1.n1 = {M1.p1.i}\")\n",
    "print(f\"Output node of port M1.n1 = {M1.p1.o}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### In a model\n",
    "\n",
    "Nodes play an important role in the `Model` class as they form the `node_type` of the underlying directed graph object. Thus, we use `Node` instances to report on graph data as well as perform operations on this graph - see [the networkx DiGraph documentation](https://networkx.github.io/documentation/stable/reference/classes/digraph.html) for details on these methods and attributes.\n",
    "\n",
    "You can also all the nodes of a given `NodeType` in a `Model` instance with (e.g. for optical nodes):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "All optical nodes in model: [<OpticalNode M1.p1.i @ 0x7f93213b9668>, <OpticalNode M1.p1.o @ 0x7f93213b9748>, <OpticalNode M1.p2.i @ 0x7f93213b9828>, <OpticalNode M1.p2.o @ 0x7f93213b98d0>, <OpticalNode M2.p1.i @ 0x7f93213d3dd8>, <OpticalNode M2.p1.o @ 0x7f93213d3e80>, <OpticalNode M2.p2.i @ 0x7f93213d3f60>, <OpticalNode M2.p2.o @ 0x7f93213d2048>]\n"
     ]
    }
   ],
   "source": [
    "print(f\"All optical nodes in model: {model.optical_nodes}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instances of nodes are used as keys in the `network` attribute of a `Model` to get the data on edges connected to the node:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'name': 'M2.p1.i->M2.p1.o', 'in_ref': <weakref at 0x7f93213df4f8; to 'OpticalNode' at 0x7f93213d3dd8>, 'out_ref': <weakref at 0x7f93213df548; to 'OpticalNode' at 0x7f93213d3e80>, 'owner': <weakref at 0x7f93213bea48; to 'Mirror' at 0x7f93213d3b70>, 'length': 1, 'coupling_type': <CouplingType.OPTICAL_TO_OPTICAL: 0>}\n"
     ]
    }
   ],
   "source": [
    "print(model.network[model.M2.p1.i.full_name][model.M2.p1.o.full_name])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What does a node contain?\n",
    "\n",
    "Similarly to a `Port`, `Node`s contain several attributes describing the properties of the node instance. The key attributes are listed below for an example of the above mirrors' port n2 output node."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Node M1.n2.o names:\n",
      "\tname = o\n",
      "\tport_name = p2.o\n",
      "\tfull_name = M1.p2.o\n",
      "Node M1.n2.o type: NodeType.OPTICAL\n",
      "Node M1.n2.o direction: NodeDirection.OUTPUT\n",
      "Node M1.n2.o component: <'M1' @ 0x7f93213b9198 (Mirror)>\n"
     ]
    }
   ],
   "source": [
    "print(\"Node M1.n2.o names:\")\n",
    "print(f\"\\tname = {model.M1.p2.o.name}\")\n",
    "print(f\"\\tport_name = {model.M1.p2.o.port_name}\")\n",
    "print(f\"\\tfull_name = {model.M1.p2.o.full_name}\")\n",
    "print(f\"Node M1.n2.o type: {model.M1.p2.o.type}\")\n",
    "print(f\"Node M1.n2.o direction: {model.M1.p2.o.direction}\")\n",
    "print(f\"Node M1.n2.o component: {model.M1.p2.o.component}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`OpticalNode` instances contain additional attributes such as the space attached to the node... "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Space linked to M1.n2.o: <'M1_M2' @ 0x7f93507d0ac8 (Space)>\n"
     ]
    }
   ],
   "source": [
    "print(f\"Space linked to M1.n2.o: {model.M1.p2.o.space}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and the `BeamParam` instance associated with the node."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "M1.p1.i.q = (-0.5+2.952624674426497j)\n"
     ]
    }
   ],
   "source": [
    "from finesse.gaussian import BeamParam\n",
    "\n",
    "model.M1.p1.i.q = BeamParam(z=-0.5, w0=1e-3)\n",
    "print(f\"M1.p1.i.q = {model.M1.p1.i.q}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note: Beam parameters are *not* stored in `OpticalNode` instances themselves, they are retrieved via the `Model` instance that the node is associated with.** See [the Finesse 3 beam parameter node usage documentation](https://finesse.readthedocs.io/en/latest/usage/advanced_usage/nodesystem/beam_parameters.html) for more details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Retrieving node paths of a model\n",
    "\n",
    "The `Model` class contains a method for getting a path between two optical nodes - `Model.path`, returning an object of type [OpticalPath](https://finesse.readthedocs.io/en/latest/api/finesse.paths.html). When `print`ed this path object displays a table showing the sequence of nodes and which component the node connects into:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "╒═════════════════════════════════════════╤══════════════════════════════════════╕\n",
      "│ From optical node                       │ Into component                       │\n",
      "╞═════════════════════════════════════════╪══════════════════════════════════════╡\n",
      "│ <OpticalNode L0.p1.o @ 0x7f9320efb390>  │ <'L0_ITM' @ 0x7f9320efb550 (Space)>  │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ITM.p1.i @ 0x7f9320efba20> │ <'ITM' @ 0x7f9320efb7b8 (Mirror)>    │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ITM.p2.o @ 0x7f9320efbba8> │ <'ITM_ETM' @ 0x7f9320efbdd8 (Space)> │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ETM.p1.i @ 0x7f9320ef7208> │ <'ETM' @ 0x7f9320efbf98 (Mirror)>    │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ETM.p2.o @ 0x7f9320ef7390> │ None                                 │\n",
      "╘═════════════════════════════════════════╧══════════════════════════════════════╛\n"
     ]
    }
   ],
   "source": [
    "path_example_model = finesse.Model()\n",
    "path_example_model.chain(\n",
    "    fc.Laser(\"L0\"), fc.Space(\"L0_ITM\"),\n",
    "    fc.Mirror(\"ITM\"), fc.Space(\"ITM_ETM\", L=1), fc.Mirror(\"ETM\")\n",
    ")\n",
    "print(path_example_model.path(\n",
    "    path_example_model.L0.p1.o, path_example_model.ETM.p2.o\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Via-nodes may also be specified such that the path traversed must pass through certain nodes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "╒═════════════════════════════════════════╤══════════════════════════════════════╕\n",
      "│ From optical node                       │ Into component                       │\n",
      "╞═════════════════════════════════════════╪══════════════════════════════════════╡\n",
      "│ <OpticalNode L0.p1.o @ 0x7f9320efb390>  │ <'L0_ITM' @ 0x7f9320efb550 (Space)>  │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ITM.p1.i @ 0x7f9320efba20> │ <'ITM' @ 0x7f9320efb7b8 (Mirror)>    │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ITM.p2.o @ 0x7f9320efbba8> │ <'ITM_ETM' @ 0x7f9320efbdd8 (Space)> │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ETM.p1.i @ 0x7f9320ef7208> │ <'ETM' @ 0x7f9320efbf98 (Mirror)>    │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ETM.p1.o @ 0x7f9320ef7278> │ <'ITM_ETM' @ 0x7f9320efbdd8 (Space)> │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ITM.p2.i @ 0x7f9320efbb38> │ <'ITM' @ 0x7f9320efb7b8 (Mirror)>    │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ITM.p2.o @ 0x7f9320efbba8> │ <'ITM_ETM' @ 0x7f9320efbdd8 (Space)> │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ETM.p1.i @ 0x7f9320ef7208> │ <'ETM' @ 0x7f9320efbf98 (Mirror)>    │\n",
      "├─────────────────────────────────────────┼──────────────────────────────────────┤\n",
      "│ <OpticalNode ETM.p2.o @ 0x7f9320ef7390> │ None                                 │\n",
      "╘═════════════════════════════════════════╧══════════════════════════════════════╛\n"
     ]
    }
   ],
   "source": [
    "print(path_example_model.path(\n",
    "    path_example_model.L0.p1.o, path_example_model.ETM.p2.o,\n",
    "    via_node=path_example_model.ITM.p2.i\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Questions/Concerns\n",
    "\n",
    "- Accessing a `Port` requires knowing the name of the port (which is assigned during construction time of each component):\n",
    "  - Is it always obvious what the port name is going to be from the component? Perhaps using \"`p<n>`\" would be more intuitive than \"`n<n>`\"?\n",
    "    - <span style=\"color:green\">**Yes - rename to \"p(n)\"**</span>\n",
    "  - Should there be an option for the user to overwrite port names?\n",
    "    - <span style=\"color:green\">**There is no need for overwriting *port* names but users should be able to tag *node* names so that they can access\n",
    "      e.g. the reflected node of a cavity with `model.nREFL` (or whatever other name they want) rather than needing to use `model.M1.n1.o`.**</span>\n",
    "- When retrieving the type of a `Port` with `Port.type` the return type is a `NodeType` instance - should `NodeType` be renamed to `PortType`?\n",
    "  - <span style=\"color:green\">**Undecided for now as we access the type via nodes as well as ports - we discussed possibly renaming to something like\n",
    "    `DomainType` (as in optical-domain, electrical-domain etc.) or `ConnectionType`.**</span>\n",
    "- The edge data of the `network` seems to contain erroneous data, e.g:\n",
    "\n",
    "```python\n",
    "model.network[model.M2.n1.i][model.M2.n1.o]\n",
    "\n",
    ">>> {'name': 'M2.n1.i->M2.n1.o', 'owner': <weakref at 0x7f6dba806958; to 'Mirror' at 0x7f6dba80d080>, 'length': 1, 'coupling_type': <CouplingType.OPTICAL_TO_OPTICAL: 0>}\n",
    "```\n",
    "\n",
    "- ... this tells us that the edge between the input and output nodes of the first port of `M1` has a length of 1 m which doesn't make sense as there is no space connecting these nodes.\n",
    "  - <span style=\"color:green\">**This is actually just the \"length\" of the edge in the directed-graph NOT the length of any space. Maybe rename this to `size`, but not so important yet\n",
    "    as we don't think this gets used anywhere right now.**</span>\n",
    "- `OpticalNode.space` is perhaps too verbose and should just be renamed to `OpticalNode.space`?\n",
    "  - <span style=\"color:green\">**Yes - rename this.**</span>\n",
    "  - **Accessing / constructing spaces is a somewhat foggy subject currently anyway, it might be worth dedicating another code review notebook to this topic so that\n",
    "    we can clear up exactly how these tasks should/could be performed**.\n",
    "    - <span style=\"color:green\">**We cleared up this point mostly - names of spaces are, for the most part, unimportant so methods such as `Model.connect` and \n",
    "    `Model.chain` allow passing of dictionaries of args or just singular attributes of spaces rather than `Space` objects themselves; where the name of the space\n",
    "    is then auto-generated from the connecting component names. If the space name is important, then one CAN also pass a `Space` object explicitly.**</span>\n",
    "    - <span style=\"color:green\">**Sean also suggested that we have a convenience function to grab the space between two components. Additionally, we can \n",
    "    easily add a `space` property to the `Port` class so that we can get a space connected to a component with, e.g., `model.ITMX.n2.space` rather than needing\n",
    "    to access via the node itself which is required currently.**</span>\n",
    "- [Model.path](https://finesse.readthedocs.io/en/latest/api/generated/finesse.model.Model.path.html#finesse.model.Model.path) is an important method for the beam tracing and will\n",
    "  likely be important for any other sub-module / extension which relies on the layout of a configuration. Due to this, does anything need to change with the API or any more information\n",
    "  need to exposed in the path object(s)? Following from this, should `Model.path` accept any type of `Node` and construct a more general `Path` object for the path between these nodes (which\n",
    "  may be of differing types)?\n",
    "  - <span style=\"color:green\">**It is difficult to know right now exactly what is required for layout-dependent sub-modules and external libraries, so the path-finding method will largely\n",
    "    be kept the same for the time being. It (hopefully) shouldn't be too difficult to allow Model.path to take two arbitrary type nodes and find a generic path between them anyway, if needed.**</span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Finesse3Kernel",
   "language": "python",
   "name": "finesse3kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
